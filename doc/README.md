Persons
=======

Overview
-----------
The application handles a registry of persons. 

It uses an imaginary downstream service _address register_, to obtain address information.

The person data is stored in an MS SQL Server database.

Use cases
---------
### Register a new person
A user (registrar) can use the application to register new persons.

The administrator enters the following information:
- First name
- Surname
- Birthdate
- Email

and request to register the person.

The application fetches address information from the _address register_ using 
the email as key and then stores the person.

After completing this use case, the person will be visible when listing all
persons.

### List all persons
A user can list all registered persons.

The application then presents a list of all registered persons with the 
following overview information:
- First name
- Surname

### View details of a specific person
A user can select a person and view detailed information.

The application then presents a details view with the following information:
- First name
- Surname
- Birthdate
- Email
- Address information

Architecture
------------
### Containers
![Containers](./containers.svg)

### Backend components
The internal architecture of the backend follows the principles of
- [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
- [Hexagonal Architecture](https://alistair.cockburn.us/hexagonal-architecture/)

Clean Arhitecture/Hexagonal Architecture/Onion Architecture advocates that
the application be structured accordingly (read more in the link above):

![The Clean Architecture](https://blog.cleancoder.com/uncle-bob/images/2012-08-13-the-clean-architecture/CleanArchitecture.jpg)

(Note that _Entities_ shall not be confused with JPA `@Entity`-annotated classes, which are part of the persistence adapter.)

In addition, when the application grows it is recommended to structure the
application in _vertical slices_ reflecting features set or maybe
bounded contexts [DDD/Bounded context](https://martinfowler.com/bliki/BoundedContext.html)


The is divided into features, or vertical slices, somewhat corresponding to bounded 
contexts and in the example there is just one: _managing persons_.

The feature of _managing persons_ is then divided into 
- _domain_
  - the core which defines the business objects and behaviours (_encapsulate 
    Enterprise wide business rules [The Clean Architecture, Robert C. Martin]_).
- _application_
  - the use cases that _orchestrate the flow of data to and from the entities, 
    and direct those entities to use their enterprise wide business rules to 
    achieve the goals of the use case [The Clean Architecture, Robert C. Martin]_.
  - exposes _in_ and _out_ as the public interface defining the interfaces that
    are used by the driving side (_in_) and the interfaces that shall be implemented 
    by the driven side (_out_)
  - do not handle logic coupled to adapter; always make the thought experiment:
    what if the application shall be driven from a CLI instead of REST,
    or what if we change persistence solution from SQL with JPA to a regular 
    file? 
- _adapters_
  - _a set of adapters that convert data from the format most convenient for 
    the use cases and entities, to the format most convenient for some external 
    agency such as the Database or the Web [The Clean Architecture, Robert C. Martin]_.
  - e.g REST controllers that drives use cases and uses the _in_ interfaces, and
    Data repositories implementing _out_ interfaces driven by the use cases.
  - should have no business logic, only logic for converting to application 
    data models and logic for handling the technology that the adapter handles,
    e.g. implement SQL queries, authentication to external system, etc.

![layers](./layers.svg)

### Code (Register Person Use Case)
The following exemplifies a specific use case (in this case _Regsister Person_)
and the actual classes involved.

![Components](./components-register-person-uc.svg)

The REST controller _RegisterPersonController_ drives the use case by calling 
the _RegisterPersonUseCase_ implemented by _RegisterPerson_.

The use case implementation _RegisterPerson_ orchestrates the behaviour:
- fetching address info using the _out_ port _FetchAddressInfo_
- creating the persisted entity using the _out_ port _CreatePerson_

_FetchAddressInfoViaWebClient_ implements _FetchAddressInfo_ and does the actual
call to the external service, as well as doing conversion of a possible external
data model to a local model

_PersonRepository_ implements _CreatePerson_ using a Spring Data Repository and
converts domain _Person_ objects to _PersonEntity_ stored in the database. 

### Mapping strategies
The book [Get Your Hands Dirty on Clean Architecture](https://www.packtpub.com/product/get-your-hands-dirty-on-clean-architecture/9781839211966)
describes various strategies regarding mapping between boundaries.

The author advocates adopting a pragmatic approach and use different strategies
depending on the underlying use case. One use case may be a simple CRUD style 
use case and there it might be appropriate to use the same implementation in all
layers, in other one might break it up and have a separate model for the commands
and use the domain model in results.

In this example application yet another dimension is added the 
[frontend-api-model](../frontend-api-model), which defines the model in the 
Web API (it corresponds to the _WebModel_ in the book). This model is defined 
outside the backend component (since it both the backend and the frontend 
depends on it). As a consequence, it makes it impossible to reuse the domain 
model in web API and necessitates the mapping between tha model (belonging to 
the Adapter layer) and the domain model and possible port models of the 
Application layer.

Apart from this the example application showcase a number of mapping strategies
- expose the domain model in the ports of the Application
- specific command models in the ports of the Application
