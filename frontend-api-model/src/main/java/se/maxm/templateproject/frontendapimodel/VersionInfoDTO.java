package se.maxm.templateproject.frontendapimodel;

import java.time.Instant;

public record VersionInfoDTO(String version, Instant created) {}
