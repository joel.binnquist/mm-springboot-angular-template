package se.maxm.templateproject.frontendapimodel;

public record AddressInfoDTO(String street, String city, String zip) {}
