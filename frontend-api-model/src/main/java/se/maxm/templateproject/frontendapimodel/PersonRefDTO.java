package se.maxm.templateproject.frontendapimodel;

import java.util.UUID;

public record PersonRefDTO(UUID id, String firstName, String surname) {}
