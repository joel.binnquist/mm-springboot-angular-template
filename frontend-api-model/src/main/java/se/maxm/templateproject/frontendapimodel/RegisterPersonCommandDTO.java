package se.maxm.templateproject.frontendapimodel;

import java.time.LocalDate;

public record RegisterPersonCommandDTO(String firstName, String surname, LocalDate birthDate, String email) {}
