package se.maxm.templateproject.frontendapimodel;

import java.time.LocalDate;
import java.util.UUID;

public record PersonDTO(
        UUID id, String firstName, String surname, LocalDate birthDate, String email, AddressInfoDTO addressInfo) {}
