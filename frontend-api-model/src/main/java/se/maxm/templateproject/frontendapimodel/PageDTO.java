package se.maxm.templateproject.frontendapimodel;

import java.util.List;

public record PageDTO<T>(int pageNumber, int requestedPageSize, long totalNumberOfItems, List<T> items) {}
