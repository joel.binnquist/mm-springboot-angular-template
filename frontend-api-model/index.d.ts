/* tslint:disable */
/* eslint-disable */
// Generated using typescript-generator version 3.1.1185 on 2022-12-27 10:43:23.

export interface PersonDTO {
    firstName: string;
    surname: string;
    age: number;
}
