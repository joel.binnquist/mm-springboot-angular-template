/* tslint:disable */
/* eslint-disable */

export interface AddressInfoDTO {
    city: string;
    street: string;
    zip: string;
}

export interface PageDTO<T> {
    items: T[];
    pageNumber: number;
    requestedPageSize: number;
    totalNumberOfItems: number;
}

export interface PersonDTO {
    addressInfo: AddressInfoDTO;
    birthDate: string;
    email: string;
    firstName: string;
    id: string;
    surname: string;
}

export interface PersonRefDTO {
    firstName: string;
    id: string;
    surname: string;
}

export interface RegisterPersonCommandDTO {
    birthDate: string;
    email: string;
    firstName: string;
    surname: string;
}

export interface VersionInfoDTO {
    created: string;
    version: string;
}
