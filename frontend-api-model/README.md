Frontend-API Model
==================
This module contains the data model exposed by the backend to the frontend.

It creates Typescript definitions from the Java classes using the 
[typescript-generator-maven-plugin](https://github.com/vojtechhabarta/typescript-generator).

Since the backend depends on the frontend (in order to include the frontend in 
the Springboot server), these data model classes needs to be broken out in a
separate package (rather than be part of the backend), so that both the 
frontend and the backend can depend on it.

The frontend includes it as a linked dependency.

When building it [index.ts](./index.ts) and [index.d.ts](./index.d.ts) are 
created. 

When changing the Java classes, remember to rebuild this module in order 
to update the _index.*_ files, so that the changes are reflected to the
frontend.
