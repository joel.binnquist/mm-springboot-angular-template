Template project for Springboot based service with Angular frontend
===================================================================
Overview
--------
This is a project that is intended to be a boiler-plate for micro services
built with a Springboot backend and (optionally) an Angular frontend.

Details of what is covered is described in each module:
- [Backend](./backend/README.md)
- [Frontend-API Data Model](./frontend-api-model/README.md)
- [Frontend](./frontend/README.md)

Application
-----------
The application implements a simple person management service in order to
showcase a number of techniques.

Se more in [Application Documentation](./doc/README.md).

Run
---
To start the application locally from the command line, make sure 
to have Java 17 or higher installed.

Build the application:
```shell
mvn clean install
```

Start the dockerized environment and init NodeRED as described below. 

Then start the application
```shell
java -Dspring.profiles.active=local \
     -Djavax.net.ssl.trustStore=/var/jsse_certs/cacerts-dev \
     -Daddress-register.base-uri="http://localhost:1880/" \
     -Dspring.datasource.h2.console.enabled=false \
     -Dspring.datasource.driver-class-name=com.microsoft.sqlserver.jdbc.SQLServerDriver \
     -Dspring.datasource.url="jdbc:sqlserver://localhost:1433;trustServerCertificate=true" \
     -Dspring.datasource.username=sa \
     -Dspring.datasource.password=Sample123$ \
     -Dspring.datasource.liquibase.user=sa \
     -Dspring.datasource.liquibase.password=Sample123$ \
     -Dspring.jpa.properties.hibernate.dialect=org.hibernate.dialect.SQLServer2012Dialect \
     -jar backend/target/templateproject-1.0.0-SNAPSHOT.jar   
```
which will start the application including the frontend and becomes available at
[local.maxm.se:9999/templateproject](http://local.maxm.se:9999/templateproject).

The application will use the H2 database (with the console available at [local.maxm.se:9999/h2-console](http://local.maxm.se:9999/h2-console))

Development environment with Docker Compose
-------------------------------------------

First install [Docker Desktop for Mac](https://docs.docker.com/desktop/install/mac-install/).

When developing locally it is then possible to use a dockerized environment
containing the following components:
- Max Security
- SQL Server
- NodeRED (to which one can import the flows in [nodered-flows.json](./nodered-flows.json)).

The docker stack is defined in [docker-compose.yml](../docker-compose.yml).

Start the stack in the same catalog as the _docker-compose.yml_ (i.e. the root 
of this repo) and execute the following:
```shell
docker-compose up
```
It is then possible to stop and restart the stack:
```shell
docker-compose stop
docker-compose start
```
(by just `stop`-ing the database contents is kept, since the docker container 
is kept).

If the database should be scratched, run:
```shell
docker-compose down
```

### Springboot
Start the Springboot backend with the following VM properties 
(in IntelliJ provide them as VM parameter in the run configuration):
```
-Daddress-register.base-uri=http://localhost:1880/
-Dspring.datasource.h2.console.enabled=false
-Dspring.datasource.driver-class-name=com.microsoft.sqlserver.jdbc.SQLServerDriver
-Dspring.datasource.url=jdbc:sqlserver://localhost:1433;trustServerCertificate=true
-Dspring.datasource.username=sa
-Dspring.datasource.password=Sample123$
-Dspring.datasource.liquibase.user=sa
-Dspring.datasource.liquibase.password=Sample123$
-Dspring.jpa.properties.hibernate.dialect=org.hibernate.dialect.SQLServer2012Dialect
```
**Note that the JDBC-URL does not contain the name of the database, 
e.g. _;databaseName=counselingmm_. Remember to use JDBC-URL above when connecting 
using a SQL client tool.**

### NodeRED
NodeRED is available at [http://localhost:1880/](http://localhost:1880/). 

However, it is initially empty, so remember to import the 
[nodered-flows.json](./nodered-flows.json) and deploy it.
 


