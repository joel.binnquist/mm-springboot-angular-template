Frontend
========
Summary
-------
A simple frontend for the _Persons Management_ feature. 
Provides a UI to be able to list persons, register new persons and view details
of a selected person.

It showcases a number of libraries and how to set up the basics of an Angular 
application using
- component routing including nested routing, see 
  - [app-routing.module.ts](./src/app/app-routing.module.ts) 
    and 
  - [persons-routing.module.ts](./src/app/pages/persons-page/persons-routing.module.ts)
- modules, see
  - [app-common.module.ts](./src/app/app-common/app-common.module.ts)
- standalone components (new for Angular 15, which makes it unnecessary to 
  declare components in module files), see any of the components, e.g.
  - [person-details.component.ts](./src/app/pages/persons-page/person-details/person-details.component.ts)
- styling utils from Bootstrap (all HTML)
- modal from _ng-bootstrap_, see
  - [register-person-modal.component.ts](./src/app/pages/persons-page/register-person-modal/register-person-modal.component.ts) 
  and 
  - [person-list.component.ts](./src/app/pages/persons-page/person-list/person-list.component.ts))
- pagination using _ng-bootstrap_, see 
  - [person-list.component.ts](./src/app/pages/persons-page/person-list/person-list.component.ts))
- Angular form validation, see 
  - [register-person-modal.component.ts](./src/app/pages/persons-page/register-person-modal/register-person-modal.component.ts))
- load spinner handling with HTTP interceptor, see
  - [loading-indicator](src/app/app-common/loading-indicator)
- setup of _MaxSecurity_, see
  - [app.module.ts](./src/app/app.module.ts)
  and
  - [max-security-setup.ts](./src/app/app-common/authentication/max-security-setup.ts)

Libraries
---------
The frontend is built on [Angular](https://angular.io).

The following libraries are used in the runtime artifact
- [Bootstrap](https://getbootstrap.com/): _Powerful, extensible, and feature-packed frontend toolkit_
- [ng-bootstrap](https://ng-bootstrap.github.io/#/home): _Angular widgets built from the ground up using Bootstrap 5 CSS with APIs designed for the Angular ecosystem_
- [MaxSecurity](https://gitlab.com/maxmatthiessen/libraries/maxsecurity-angular-client): Angular adaptation for _MaxSecurity_

For development and test the following libraries are used:
- [Lodash](https://lodash.com/): _Lodash makes JavaScript easier by taking the hassle out of working with arrays, numbers, objects, strings, etc._
- [ESLint](https://eslint.org/): _Find and fix problems in your JavaScript/Typescript code_
- [Jest](https://jestjs.io): _Jest is a delightful JavaScript Testing Framework with a focus on simplicity_
- [Testing Library](https://testing-library.com/docs/angular-testing-library/intro):
  _Simple and complete testing utilities that encourage good testing practices_
- [Storybook](https://storybook.js.org/docs/angular/get-started/introduction): 
  _frontend workshop for building UI components and pages in isolation_

Node and Yarn
-------------
A local Node and Yarn is installed by Maven (by the 
_frontend-maven-plugin_ plugin) in the _initialize_ phase, thus when doing
```shell
mvn initialize
```

They will be installed in [./node](./node), and it is recommended to add
it firts in your PATH: e.g.
```shell
export PATH=./node:$PATH
```
To ensure that these versions are used when running yarn commands from the 
command line. 

It is also recommended to reference these in project local settings for NodeJS in 
Intellij IDEA (see _Preferences | Languages & Frameworks | Node.js_), so that 
they are used in run configurations.

API Type Definitions
--------------------
The type definitions generated in [frontend-api-model](../frontend-api-model)
are included as a linked dependency in [package.json](./package.json):
```json
:
"frontend-api-model": "link:../frontend-api-model",
:
```

In the application these are imported as in the following example:
```typescript
import { PersonDTO } from "frontend-api-model";
:
```


Develop
-------
See the `script` section of [package.json](./package.json) for all available 
commands. Below are the most important.

### Storybook
When developing components, it is recommended to use _Storybook_.

Start the Storybook server with
```shell
yarn storybook
```
and open it on [localhost:6006](http://localhost:6006).

### Development server
It is also possible to start the entire application running the webpack development 
server with
```shell
yarn start
```
and open it on [local.maxm.se:4299](http://local.maxm.se:4299).

It is configured in [proxy.conf.json](./proxy.conf.json) to proxy 
`/templateproject/**` requests to the Springboot backend at
[local.maxm.se:9999](http://local.maxm.se:9999). 

### Testing
Tests are executed with the _Jest_ test runner. It can be done on the 
command line with 
```shell
yarn test
```
or directly in Intellij IDEA.

It is also possible to have the tests running continuously in watch mode with
```shell
yarn test:watch
```

In addition to running the tests specified in _*.spec.ts_ files the test runner
also runs 
[snapshot tests](https://storybook.js.org/docs/angular/writing-tests/snapshot-testing).
When these fails one should check the indicated diff and verify that is valid
(remember that snapshot test fails when making intentional changes in components).
If the diff is OK, the snapshot can be updated with 
```shell
yarn test:update-snapshot
```
(Note, this can be done directly in the test output pane of Intellij IDEA).

Build
-----
The final artifact is built using Maven:
```shell
mvn install
```
which builds the application with the _production_ profile as default and 
packages it in a _.jar_-file (which is included in _backend_).

It is possible to configure the maven build as follows
- `-DskipTests`: will skip tests as usual
- `-Dfrontend.build.argument`: defaults to `build:prod`, but makes it possible
  to use just `build` instead if one does not want the _production_ profile 
  (e.g. to disable minification)

### Base HREF
Note that the `--base-href` option of the `build` commands must match the 
context of the backend specified in [application.yml](../backend/src/main/resources/application.yml).
