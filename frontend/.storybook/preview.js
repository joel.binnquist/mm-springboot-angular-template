import "@storybook/addon-console";
import '@angular/localize/init'; // Needed for ng-bootstrap
import {componentWrapperDecorator} from '@storybook/angular';


// Provide the MSW addon decorator globally


export const decorators = [
  componentWrapperDecorator((story) => `<div style="background: white; border: 1px dashed indianred">${story}</div>`),
];

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  docs: { inlineStories: true },
}
