import { NgModule } from "@angular/core";
import { Route, RouterModule } from "@angular/router";
import { MaxSecurityAuthGuard } from "@maxm/maxsecurity-angular-client";
import { PersonDetailsComponent } from "./person-details/person-details.component";
import { PersonListComponent } from "./person-list/person-list.component";
import { PersonsPageComponent } from "./persons-page.component";

export const PERSON_PAGE_SUBROUTES: Array<Route> = [
  {
    path: "persons",
    component: PersonsPageComponent,
    children: [
      { path: "", pathMatch: "prefix", redirectTo: "list" },
      { path: "list", component: PersonListComponent, canActivate: [MaxSecurityAuthGuard] },
      { path: ":id", component: PersonDetailsComponent, canActivate: [MaxSecurityAuthGuard] },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(PERSON_PAGE_SUBROUTES)],
})
export class PersonsRoutingModule {}
