import { Component } from "@angular/core";
import { RouterModule } from "@angular/router";

@Component({
  selector: "mm-landing-page",
  template: `
    <div class="d-flex flex-column p-2 gap-2">
      <h2 data-testid="landing-page-header">Hantera personer</h2>
      <router-outlet></router-outlet>
    </div>
  `,
  standalone: true,
  imports: [RouterModule],
})
export class PersonsPageComponent {}
