import { Component } from "@angular/core";

import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { NgbModal, NgbModalModule, NgbPaginationModule } from "@ng-bootstrap/ng-bootstrap";
import { PageDTO, PersonRefDTO, RegisterPersonCommandDTO } from "frontend-api-model";
import { finalize, mergeMap } from "rxjs";
import { BackendApiService } from "../../../app-common/backend-api/backend-api.service";
import { RegisterPersonModalComponent } from "../register-person-modal/register-person-modal.component";

@Component({
  selector: "mm-person-list",
  template: `
    <div *ngIf="page" class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr class="align-middle">
            <th scope="col">Förnamn</th>
            <th scope="col">Efternamn</th>
            <th scope="col" class="min">
              <button
                type="button"
                class="btn btn-outline-primary btn-sm pull-right"
                [disabled]="saving"
                (click)="openAddPersonModal()"
              >
                Registrera person
              </button>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr *ngFor="let person of page.items">
            <td>{{ person.firstName }}</td>
            <td>{{ person.surname }}</td>
            <td class="text-center"><a [routerLink]="'/persons/' + person.id">Öppna</a></td>
          </tr>
        </tbody>
      </table>
      <div class="d-flex justify-content-between">
        <ul class="pagination pagination-sm">
          <li *ngFor="let size of pageSizes" class="page-item" [class.active]="pageSize === size">
            <button class="btn-link page-link" (click)="setPageSize(size)">{{ size }}</button>
          </li>
        </ul>
        <ngb-pagination
          [page]="page.pageNumber + 1"
          (pageChange)="loadPage($event)"
          [pageSize]="pageSize"
          [collectionSize]="page.totalNumberOfItems"
          size="sm"
        ></ngb-pagination>
      </div>
    </div>
  `,
  standalone: true,
  imports: [CommonModule, NgbModalModule, NgbPaginationModule, RouterModule],

  styles: [
    `
      th.min {
        width: 1%;
        white-space: nowrap;
      }
    `,
  ],
})
export class PersonListComponent {
  readonly pageSizes = [10, 20, 50];
  pageSize = this.pageSizes[0];
  saving = false;
  page?: PageDTO<PersonRefDTO>;

  constructor(private readonly modalService: NgbModal, private readonly backendApi: BackendApiService) {
    this.backendApi.getAllPersons(0, this.pageSize).subscribe((page) => (this.page = page));
  }

  async openAddPersonModal() {
    const ref = this.modalService.open(RegisterPersonModalComponent);
    try {
      const modalResult: RegisterPersonCommandDTO = await ref.result;
      console.log("Register person", modalResult);
      this.saving = true;
      this.backendApi
        .registerPerson(modalResult)
        .pipe(
          mergeMap(() => this.backendApi.getAllPersons(this.page?.pageNumber ?? 0, this.pageSize)),
          finalize(() => (this.saving = false))
        )
        .subscribe((page) => (this.page = page));
    } catch (dismiss) {
      console.log("Modal dismissed:", dismiss);
    }
  }

  loadPage(requestedPage: number) {
    this.backendApi.getAllPersons(requestedPage - 1, this.pageSize).subscribe((page) => (this.page = page));
  }

  setPageSize(size: number) {
    this.pageSize = size;
    this.loadPage(1);
  }
}
