import { RouterTestingModule } from "@angular/router/testing";
import { Meta, moduleMetadata, Story } from "@storybook/angular";
import { userEvent, within } from "@storybook/testing-library";
import { from as observableFrom, of as observableOf } from "rxjs";
import { BackendApiService } from "../../../app-common/backend-api/backend-api.service";
import { PERSONS_PAGE } from "../__mockdata__";
import { PersonListComponent } from "./person-list.component";

export default {
  title: "TemplateProject/pages/persons-page/PersonListComponent",
  component: PersonListComponent,
  decorators: [
    moduleMetadata({
      imports: [RouterTestingModule],
      providers: [
        {
          provide: BackendApiService,
          useValue: {
            getAllPersons: () => observableOf(PERSONS_PAGE),
          } as Partial<BackendApiService>,
        },
      ],
    }),
  ],
} as Meta<PersonListComponent>;

const Template: Story<PersonListComponent> = (args: PersonListComponent) => ({
  props: { ...args },
});

export const WithNoPageLoaded = Template.bind({});
WithNoPageLoaded.decorators = [
  moduleMetadata({
    providers: [
      {
        provide: BackendApiService,
        useValue: {
          getAllPersons: () => observableFrom([]),
        } as Partial<BackendApiService>,
      },
    ],
  }),
];

export const WithAPageLoaded = Template.bind({});
WithAPageLoaded.args = {};

export const OpenRegisterAPersonModal = Template.bind({});
OpenRegisterAPersonModal.play = async ({ canvasElement }) => {
  const pageObject = new PageObject(canvasElement);
  await pageObject.clickRegistreraPersonButton();
};
class PageObject {
  constructor(private readonly canvasElement: HTMLElement) {}

  async clickRegistreraPersonButton() {
    const button = await within(this.canvasElement).getByText("Registrera person", { selector: "button" });
    await userEvent.click(button);
  }
}
