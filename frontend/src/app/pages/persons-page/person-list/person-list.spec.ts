import "@angular/localize/init";
import { render, RenderResult } from "@testing-library/angular";
import { createMock } from "@testing-library/angular/jest-utils";
import { of as observableOf } from "rxjs";
import { BackendApiService } from "../../../app-common/backend-api/backend-api.service";
import { pause } from "../../../__testutil__/test-utils";
import { PERSONS_PAGE } from "../__mockdata__";
import { PersonListComponent } from "./person-list.component";
import { OpenRegisterAPersonModal } from "./person-list.stories";

describe("PersonListComponent", () => {
  const backendApiMock = createMock(BackendApiService);
  let renderResult: RenderResult<PersonListComponent>;
  describe("when clicking the 'Registrera person' button (OpenRegisterAPersonModal interaction)", () => {
    beforeEach(async () => {
      backendApiMock.getAllPersons.mockImplementation(() => observableOf(PERSONS_PAGE));
      renderResult = await render(PersonListComponent, {
        componentProperties: OpenRegisterAPersonModal.args,
        providers: [
          {
            provide: BackendApiService,
            useValue: backendApiMock,
          },
        ],
      });

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      await (OpenRegisterAPersonModal as any).play({ canvasElement: renderResult.container });
      pause(10);
    });

    it("it should open the modal for registering a person", () => {
      expect(window.document.body).toMatchSnapshot(); // Need to check body since it is a modal
    });
  });
});
