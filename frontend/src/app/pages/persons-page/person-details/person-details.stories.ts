import { ActivatedRoute } from "@angular/router";
import { Meta, moduleMetadata, Story } from "@storybook/angular";
import { from as observableFrom, of as observableOf } from "rxjs";
import { BackendApiService } from "../../../app-common/backend-api/backend-api.service";
import { PERSON } from "../__mockdata__";
import { PersonDetailsComponent } from "./person-details.component";

export default {
  title: "TemplateProject/pages/persons-page/PersonDetailsComponent",
  component: PersonDetailsComponent,
  decorators: [
    moduleMetadata({
      providers: [
        {
          provide: BackendApiService,
          useValue: {
            getPerson: () => observableOf(PERSON),
          } as Partial<BackendApiService>,
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: observableFrom([{ id: PERSON.id }]),
          },
        },
      ],
    }),
  ],
} as Meta<PersonDetailsComponent>;

const Template: Story<PersonDetailsComponent> = (args: PersonDetailsComponent) => ({
  props: { ...args },
});

export const WithPersonLoaded = Template.bind({});
WithPersonLoaded.args = {};

export const FailedToLoadPerson = Template.bind({});
FailedToLoadPerson.decorators = [
  moduleMetadata({
    providers: [
      {
        provide: ActivatedRoute,
        useValue: {
          params: observableFrom([{}]),
        },
      },
    ],
  }),
];
