import { CommonModule } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, RouterModule } from "@angular/router";
import { PersonDTO } from "frontend-api-model";
import { mergeMap, Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { BackendApiService } from "../../../app-common/backend-api/backend-api.service";
import { PersonPropertiesComponent } from "./person-properties.component";

@Component({
  selector: "mm-person-details",
  template: `
    <div class="card d-flex flex-column gap-2 p-3">
      <a routerLink="/persons/list" class="mb-3">Tillbaka till listan</a>
      <div *ngIf="person$ | async as person">
        <h5>Detaljer för {{ person.firstName }}</h5>
        <mm-person-properties [person]="person"></mm-person-properties>
      </div>
      <div *ngIf="loadError" class="alert alert-danger">Det gick inte att ladda angiven person.</div>
    </div>
  `,
  standalone: true,
  imports: [CommonModule, RouterModule, PersonPropertiesComponent, RouterModule],
})
export class PersonDetailsComponent implements OnInit {
  person$?: Observable<PersonDTO>;
  loadError = false;
  constructor(private readonly backendApi: BackendApiService, private readonly route: ActivatedRoute) {}

  ngOnInit(): void {
    this.person$ = this.route.params.pipe(
      mergeMap((map) => {
        const id = map["id"];
        if (!id) {
          return throwError(() => new Error("No :id provided on the route"));
        }
        return this.backendApi.getPerson(id);
      }),
      catchError((e) => {
        this.loadError = true;
        return throwError(() => e);
      })
    );
  }
}
