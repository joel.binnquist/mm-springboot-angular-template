import { DatePipe } from "@angular/common";
import { Component, Input } from "@angular/core";
import { PersonDTO } from "frontend-api-model";

@Component({
  selector: "mm-person-properties",
  template: `
    <dl class="row">
      <dt class="col-sm-3 text-truncate">Förnamn</dt>
      <dd class="col-sm-9">{{ person?.firstName }}</dd>
      <dt class="col-sm-3">Efternamn</dt>
      <dd class="col-sm-9">{{ person?.surname }}</dd>
      <dt class="col-sm-3">Födelsedatum</dt>
      <dd class="col-sm-9">{{ person?.birthDate | date : "yyyy-mm-dd" }}</dd>
      <dt class="col-sm-3 text-truncate">E-post</dt>
      <dd class="col-sm-9">{{ person?.email }}</dd>
      <dt class="col-sm-3 text-truncate">ID</dt>
      <dd class="col-sm-9">{{ person?.id }}</dd>
    </dl>
    <h6 class="mt-2">Adress</h6>
    <dl class="row">
      <dt class="col-sm-3 text-truncate">Gata</dt>
      <dd class="col-sm-9">{{ person?.addressInfo?.street }}</dd>
      <dt class="col-sm-3 text-truncate">Postort</dt>
      <dd class="col-sm-9">{{ person?.addressInfo?.city }}</dd>
      <dt class="col-sm-3 text-truncate">Postnummer</dt>
      <dd class="col-sm-9">{{ person?.addressInfo?.zip }}</dd>
    </dl>
  `,
  standalone: true,
  imports: [DatePipe],
})
export class PersonPropertiesComponent {
  @Input()
  person?: PersonDTO;
}
