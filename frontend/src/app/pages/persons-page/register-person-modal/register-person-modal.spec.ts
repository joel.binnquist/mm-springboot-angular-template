import { CommonModule } from "@angular/common";
import "@angular/localize/init";
import { NgbModalModule } from "@ng-bootstrap/ng-bootstrap";
import { render, RenderResult } from "@testing-library/angular";
import { pause } from "../../../__testutil__/test-utils";
import { ModalTriggerComponent } from "../__mockdata__";
import { FillInModal, InvalidValues, RegisterAPerson } from "./register-person-modal.stories";

describe("AddPersonModalComponent", () => {
  let renderResult: RenderResult<ModalTriggerComponent>;
  describe("when filling in the modal with valid values (FillInModal interaction)", () => {
    beforeEach(async () => {
      renderResult = await render(ModalTriggerComponent, {
        imports: [CommonModule, NgbModalModule],
      });

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      await (FillInModal as any).play({ canvasElement: renderResult.container });
      pause(10);
    });

    it("it should match the previous snapshot", () => {
      expect(window.document.body).toMatchSnapshot(); // Need to check body since it is a modal
    });
  });

  describe("when filling in the modal with invalid values (InvalidValues interaction)", () => {
    beforeEach(async () => {
      renderResult = await render(ModalTriggerComponent, {
        imports: [CommonModule, NgbModalModule],
      });

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      await (InvalidValues as any).play({ canvasElement: renderResult.container });
      pause(10);
    });

    it("it should match the previous snapshot", () => {
      expect(window.document.body).toMatchSnapshot(); // Need to check body since it is a modal
    });
  });

  describe("when registering a person in the modal with invalid values (RegisterAPerson interaction)", () => {
    beforeEach(async () => {
      renderResult = await render(ModalTriggerComponent, {
        imports: [CommonModule, NgbModalModule],
      });

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      await (RegisterAPerson as any).play({ canvasElement: renderResult.container });
      pause(10);
      renderResult.detectChanges();
    });

    it("it should match the previous snapshot", async () => {
      expect(window.document.body).toMatchSnapshot(); // Need to check body since it is a modal
    });
    it("it should emit a the filled in values as result", async () => {
      const result = await renderResult.fixture.componentInstance.modalResult;
      expect(result).toEqual({
        birthDate: "1900-01-01",
        email: "kaka.banan@bananbolaget.se",
        firstName: "Kaka",
        surname: "Banan",
      });
    });
  });
});
