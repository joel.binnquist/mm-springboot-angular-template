import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { NgbActiveModal, NgbModalModule } from "@ng-bootstrap/ng-bootstrap";
import { RegisterPersonCommandDTO } from "frontend-api-model";

@Component({
  selector: "mm-add-person-modal",
  template: `
    <div class="d-flex flex-column p-2">
      <div class="modal-header">
        <div class="fs-4 modal-title pull-left">Registrera ny person</div>
        <button type="button" class="btn-close close pull-right" aria-label="Close" (click)="cancel()">
          <span aria-hidden="true" class="visually-hidden">&times;</span>
        </button>
      </div>

      <form class="modal-body d-flex flex-column gap-2" #form="ngForm">
        <!--        The inputs below should be refactored into components -->
        <div class="form-floating">
          <input
            type="text"
            class="form-control"
            id="firstNameInput"
            name="firstName"
            placeholder="Ange förnamn"
            [(ngModel)]="firstName"
            required
            #firstNameInput="ngModel"
            ngbAutoFocus
          />
          <label for="firstNameInputInput" class="form-label">Förnamn</label>
          <div *ngIf="firstNameInput.invalid && firstNameInput.touched" class="text-danger">
            <div *ngIf="firstNameInput.errors?.['required']">Förnamn måste anges</div>
          </div>
        </div>
        <div class="form-floating">
          <input
            type="text"
            class="form-control"
            id="surnameInput"
            name="surname"
            placeholder="Ange efternamn"
            [(ngModel)]="surname"
            required
            #surnameInput="ngModel"
          />
          <label for="surnameInputInput" class="form-label">Efternamn</label>
          <div *ngIf="surnameInput.invalid && surnameInput.touched" class="text-danger">
            <div *ngIf="surnameInput.errors?.['required']">Efternamn måste anges</div>
          </div>
        </div>
        <div class="form-floating">
          <input
            type="date"
            class="form-control"
            id="birthDateInput"
            name="birthDate"
            placeholder="Ange födelsedatum"
            [(ngModel)]="birthDate"
            required
            #birthDateInput="ngModel"
          />
          <label for="birthDateInput" class="form-label">Födelsedatum</label>
          <div *ngIf="birthDateInput.invalid && birthDateInput.touched" class="text-danger">
            <div *ngIf="birthDateInput.errors?.['required']">Födelsedatum måste anges</div>
          </div>
        </div>
        <div class="form-floating">
          <input
            type="email"
            class="form-control"
            id="emailInput"
            name="email"
            placeholder="xx.yy@mail.com"
            [(ngModel)]="emailAddress"
            required
            email
            #emailInput="ngModel"
          />
          <label for="emailInput" class="form-label">E-post</label>
          <div *ngIf="emailInput.invalid && emailInput.touched" class="text-danger">
            <div *ngIf="emailInput.errors?.['required']">E-post måste anges</div>
            <div *ngIf="emailInput.errors?.['email']">E-post har felaktigt format</div>
          </div>
        </div>
      </form>

      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary align-self-end" aria-label="Cancel" (click)="cancel()">
          Avbryt
        </button>
        <button
          type="button"
          class="btn btn-primary align-self-end"
          aria-label="Create"
          [disabled]="form.invalid"
          (click)="create()"
        >
          Registrera
        </button>
      </div>
    </div>
  `,
  standalone: true,
  imports: [NgbModalModule, FormsModule, CommonModule],
})
export class RegisterPersonModalComponent {
  firstName?: string;
  surname?: string;
  emailAddress?: string;
  birthDate?: string;

  constructor(private readonly activeModal: NgbActiveModal) {}
  create() {
    if (this.firstName && this.surname && this.emailAddress && this.birthDate) {
      this.activeModal.close({
        firstName: this.firstName,
        surname: this.surname,
        email: this.emailAddress,
        birthDate: this.birthDate,
      } as RegisterPersonCommandDTO);
    }
  }

  cancel() {
    this.activeModal.dismiss("AddPersonModalComponent cancelled");
  }
}
