import { CommonModule } from "@angular/common";
import { NgbModalModule } from "@ng-bootstrap/ng-bootstrap";
import { componentWrapperDecorator, Meta, moduleMetadata, Story } from "@storybook/angular";
import { userEvent, within } from "@storybook/testing-library";
import { ModalTriggerComponent } from "../__mockdata__";
import { RegisterPersonModalComponent } from "./register-person-modal.component";

export default {
  title: "TemplateProject/pages/persons-page/RegisterPersonModalComponent",
  component: RegisterPersonModalComponent,
  decorators: [
    moduleMetadata({
      imports: [CommonModule, NgbModalModule],
      declarations: [ModalTriggerComponent],
    }),
    componentWrapperDecorator(() => `<mm-modal-trigger></mm-modal-trigger>`),
  ],
} as Meta<RegisterPersonModalComponent>;
const Template: Story<RegisterPersonModalComponent> = (args: RegisterPersonModalComponent) => ({
  props: { ...args },
});

export const FillInModal = Template.bind({});
FillInModal.play = async () => {
  const pageObject = new PageObject();
  await pageObject.fillInRegisterPersonModal();
};
export const InvalidValues = Template.bind({});
InvalidValues.play = async () => {
  const pageObject = new PageObject();
  await pageObject.fillInInvalidRegisterPersonModal();
};

export const RegisterAPerson = Template.bind({});
RegisterAPerson.play = async () => {
  const pageObject = new PageObject();
  await pageObject.fillInRegisterPersonModal();
  await pageObject.submitRegisterPersonModal();
};

class PageObject {
  async fillInRegisterPersonModal() {
    // Cannot search within canvasElement for elements in the modal, since the modal is mounted directly on the body
    const firstNameInput = await within(window.document.body).getByPlaceholderText("Ange förnamn");
    await userEvent.type(firstNameInput, "Kaka");
    const surnameInput = await within(window.document.body).getByPlaceholderText("Ange efternamn");
    await userEvent.type(surnameInput, "Banan");
    const birthDateInput = await within(window.document.body).getByPlaceholderText("Ange födelsedatum");
    await userEvent.type(birthDateInput, "1900-01-01");
    const emailInput = await within(window.document.body).getByPlaceholderText("xx.yy@mail.com");
    await userEvent.type(emailInput, "kaka.banan@bananbolaget.se");
  }
  async fillInInvalidRegisterPersonModal() {
    // Cannot search within canvasElement for elements in the modal, since the modal is mounted directly on the body
    const firstNameInput = await within(window.document.body).getByPlaceholderText("Ange förnamn");
    await userEvent.type(firstNameInput, "Kaka");
    await userEvent.clear(firstNameInput);
    const surnameInput = await within(window.document.body).getByPlaceholderText("Ange efternamn");
    await userEvent.type(surnameInput, "Banan");
    await userEvent.clear(surnameInput);
    const birthDateInput = await within(window.document.body).getByPlaceholderText("Ange födelsedatum");
    await userEvent.type(birthDateInput, "1900-01-01");
    await userEvent.clear(birthDateInput);
    const emailInput = await within(window.document.body).getByPlaceholderText("xx.yy@mail.com");
    await userEvent.type(emailInput, "inte en mäjl-adress");
    await userEvent.click(firstNameInput);
  }

  async submitRegisterPersonModal() {
    // Cannot search within canvasElement for elements in the modal, since the modal is mounted directly on the body
    const registreraPersonButton = await within(window.document.body).getByText("Registrera", { selector: "button" });
    await userEvent.click(registreraPersonButton);
  }
}
