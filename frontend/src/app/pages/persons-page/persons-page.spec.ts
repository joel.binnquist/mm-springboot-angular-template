import { render, screen } from "@testing-library/angular";
import { PersonsPageComponent } from "./persons-page.component";
import { PersonsRoutingModule } from "./persons-routing.module";

describe("ModelPortfolioHomePageComponent", () => {
  beforeEach(async () => {
    await render(PersonsPageComponent, {
      imports: [PersonsRoutingModule],
      providers: [],
      declarations: [],
      componentProperties: {},
    });
  });

  describe("Checks the title component", () => {
    it("checks the value of the Title component", () => {
      const titleValue = screen.getByTestId("landing-page-header");
      expect(titleValue).toBeInTheDocument();
    });
  });
});
