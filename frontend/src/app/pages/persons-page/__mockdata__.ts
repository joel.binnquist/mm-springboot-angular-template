import { Component, OnInit } from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { PageDTO, PersonDTO, PersonRefDTO, RegisterPersonCommandDTO } from "frontend-api-model";
import { RegisterPersonModalComponent } from "./register-person-modal/register-person-modal.component";

export const PERSON: PersonDTO = {
  firstName: "Kaka",
  surname: "Banan",
  birthDate: "1900-01-01",
  email: "kaka.banan@bageriet.se",
  id: "d05dc6a1-36be-4c1d-8beb-75d9443d3a29",
  addressInfo: {
    street: "Gatan 2",
    city: "Staden",
    zip: "42424",
  },
};

export const PERSONS_PAGE = {
  pageNumber: 2,
  requestedPageSize: 20,
  totalNumberOfItems: 43,
  items: [
    {
      firstName: "Kaka",
      surname: "Banan",
      id: "d05dc6a1-36be-4c1d-8beb-75d9443d3a29",
    },
    {
      firstName: "Hubba",
      surname: "Bubba",
      id: "138db93d-277d-43a1-b7c5-16e1b29cd39c",
    },
    {
      firstName: "Bulle",
      surname: "Deg",
      id: "8d73b8c3-263a-4f63-932f-13de07980e91",
    },
  ],
} as PageDTO<PersonRefDTO>;

/**
 * This component is only used to trigger the opening of the modal.
 */
@Component({
  selector: "mm-modal-trigger",
  template: `
    <h4>Modal trigger</h4>
    <div>Modal result: {{ modalResult | json }}</div>
  `,
})
export class ModalTriggerComponent implements OnInit {
  modalRef?: NgbModalRef;
  modalResult?: RegisterPersonCommandDTO;

  constructor(private readonly modalService: NgbModal) {}

  async ngOnInit() {
    this.modalRef = this.modalService.open(RegisterPersonModalComponent);
    this.modalResult = await this.modalRef.result;
  }
}
