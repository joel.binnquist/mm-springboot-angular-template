import { RouterTestingModule } from "@angular/router/testing";
import { componentWrapperDecorator, Meta, moduleMetadata, Story } from "@storybook/angular";
import { userEvent, within } from "@storybook/testing-library";
import { of as observableOf } from "rxjs";
import { BackendApiService } from "../../app-common/backend-api/backend-api.service";
import { PersonListComponent } from "./person-list/person-list.component";
import { PersonsPageComponent } from "./persons-page.component";
import { PERSONS_PAGE } from "./__mockdata__";

export default {
  title: "TemplateProject/pages/persons-page/PersonsPageComponent",
  component: PersonsPageComponent,
  decorators: [
    moduleMetadata({
      imports: [
        RouterTestingModule.withRoutes([{ path: "list", component: PersonListComponent }], {
          useHash: true,
        }),
      ],
      providers: [
        {
          provide: BackendApiService,
          useValue: {
            getAllPersons: () => observableOf(PERSONS_PAGE),
          } as Partial<BackendApiService>,
        },
      ],
    }),
    componentWrapperDecorator(
      (story) => `
    <div>
      <a routerLink="/list">Öppna lista</a>
      ${story}
    </div>
    `
    ),
  ],
} as Meta<PersonsPageComponent>;

const Template: Story<PersonsPageComponent> = (args: PersonsPageComponent) => ({
  props: { ...args },
});

export const RouteToList = Template.bind({});
RouteToList.play = async ({ canvasElement }) => {
  const pageObject = new PageObject(canvasElement);
  await pageObject.navigateToList();
};
class PageObject {
  constructor(private readonly canvasElement: HTMLElement) {}

  async navigateToList() {
    const link = await within(this.canvasElement).getByText("Öppna lista", { selector: "a" });
    await userEvent.click(link);
  }
}
