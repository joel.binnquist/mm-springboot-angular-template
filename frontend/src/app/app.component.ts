import { Component } from "@angular/core";
import { debounceTime, map } from "rxjs/operators";
import { BackendApiService } from "./app-common/backend-api/backend-api.service";
import { HttpLoadingService } from "./app-common/loading-indicator/http-loading.service";

@Component({
  selector: "mm-root",
  template: `
    <mm-spinner *ngIf="httpLoading$ | async"></mm-spinner>
    <main class="container bg-white flex-shrink-0 flex-grow-1">
      <div class="d-flex flex-column py-2 gap-3">
        <h1 class="text-center">Mallprojektet</h1>
        <div>
          <router-outlet></router-outlet>
        </div>
      </div>
    </main>
    <footer class="footer">
      <div class="container p-1 bg-white text-center text-muted">
        {{ version$ | async }}
      </div>
    </footer>
  `,
})
export class AppComponent {
  readonly httpLoading$ = this.httpLoadingService.loading$().pipe(debounceTime(500));
  readonly version$ = this.backendApiService.getVersionInfo().pipe(
    map((versionInfo) => {
      const createdDate = new Date(versionInfo.created);
      return `ver: ${
        versionInfo.version
      } skapad: ${createdDate.toLocaleDateString()} ${createdDate.toLocaleTimeString()}`;
    })
  );

  constructor(
    private readonly httpLoadingService: HttpLoadingService,
    private readonly backendApiService: BackendApiService
  ) {}
}
