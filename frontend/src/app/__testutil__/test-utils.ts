export async function pause(millis: number) {
  return new Promise<void>((resolve) => setTimeout(() => resolve(), millis));
}
