import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import {
  MaxSecurityClientModule,
  MaxSecurityTokenPersistService,
  MAXSECURITY_CLIENT,
} from "@maxm/maxsecurity-angular-client";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AppCommonModule } from "./app-common/app-common.module";
import { authClientFactory } from "./app-common/authentication/max-security-setup";
import { SpinnerComponent } from "./app-common/loading-indicator/spinner.component";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PersonsRoutingModule } from "./pages/persons-page/persons-routing.module";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PersonsRoutingModule,
    NgbModule,
    AppCommonModule,
    MaxSecurityClientModule,
    SpinnerComponent,
  ],
  providers: [
    {
      provide: MAXSECURITY_CLIENT,
      useFactory: authClientFactory,
      deps: [MaxSecurityTokenPersistService],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
