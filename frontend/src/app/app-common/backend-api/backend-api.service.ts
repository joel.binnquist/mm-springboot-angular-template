import { HttpClient } from "@angular/common/http";

import { Injectable } from "@angular/core";
import { PageDTO, PersonDTO, PersonRefDTO, RegisterPersonCommandDTO, VersionInfoDTO } from "frontend-api-model";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class BackendApiService {
  constructor(private readonly httpClient: HttpClient) {}

  registerPerson(registerPersonCommand: RegisterPersonCommandDTO): Observable<void> {
    return this.httpClient.post<void>("/templateproject/api/persons", registerPersonCommand);
  }

  getAllPersons(pageNumber: number, requestedPageSize: number): Observable<PageDTO<PersonRefDTO>> {
    return this.httpClient.get<PageDTO<PersonRefDTO>>(
      `/templateproject/api/persons?pageNumber=${pageNumber}&requestedPageSize=${requestedPageSize}`
    );
  }

  getPerson(id: string): Observable<PersonDTO> {
    return this.httpClient.get<PersonDTO>(`/templateproject/api/persons/${id}`);
  }

  getVersionInfo(): Observable<VersionInfoDTO> {
    return this.httpClient.get<VersionInfoDTO>(`/templateproject/sysinfo/version`);
  }
}
