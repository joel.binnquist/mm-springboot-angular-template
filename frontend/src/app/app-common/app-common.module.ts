import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BackendApiService } from "./backend-api/backend-api.service";
import { HttpLoadingInterceptor } from "./loading-indicator/http-loading.interceptor";

@NgModule({
  imports: [HttpClientModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpLoadingInterceptor,
      multi: true,
    },
    BackendApiService,
  ],
})
export class AppCommonModule {}
