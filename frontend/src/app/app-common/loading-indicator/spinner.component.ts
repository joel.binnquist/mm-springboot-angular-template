import { Component } from "@angular/core";

@Component({
  selector: "mm-spinner",
  styles: [
    `
      .centered-overlay {
        position: fixed;
        height: 100vh;
        width: 100vw;
        z-index: 10000;
        background-color: rgba(0, 0, 0, 0.2);
      }
    `,
  ],
  template: `
    <div class="centered-overlay d-flex justify-content-center align-items-center">
      <div class="spinner-border" role="status">
        <span class="visually-hidden">Laddar...</span>
      </div>
    </div>
  `,
  standalone: true,
})
export class SpinnerComponent {}
