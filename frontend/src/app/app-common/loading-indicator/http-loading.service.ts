import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class HttpLoadingService {
  private readonly loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private readonly currentlyLoadingUrls: Map<string, boolean> = new Map<string, boolean>();

  loading$(): Observable<boolean> {
    return this.loadingSubject.asObservable();
  }
  setLoading(loading: boolean, url: string): void {
    if (!url) {
      throw new Error("The request URL must be provided to the LoadingService.setLoading function");
    }
    if (loading) {
      this.currentlyLoadingUrls.set(url, loading);
      this.loadingSubject.next(true);
    } else if (!loading && this.currentlyLoadingUrls.has(url)) {
      this.currentlyLoadingUrls.delete(url);
    }
    if (this.currentlyLoadingUrls.size === 0) {
      this.loadingSubject.next(false);
    }
  }
}
