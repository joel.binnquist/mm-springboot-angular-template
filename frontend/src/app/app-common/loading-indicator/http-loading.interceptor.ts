import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { HttpLoadingService } from "./http-loading.service";

@Injectable()
export class HttpLoadingInterceptor implements HttpInterceptor {
  constructor(private readonly httpLoadingService: HttpLoadingService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    this.httpLoadingService.setLoading(true, request.url);
    return next
      .handle(request)
      .pipe(
        catchError((err) => {
          this.httpLoadingService.setLoading(false, request.url);
          return throwError(err);
        })
      )
      .pipe(
        map((evt: HttpEvent<unknown>) => {
          if (evt instanceof HttpResponse) {
            this.httpLoadingService.setLoading(false, request.url);
          }
          return evt;
        })
      );
  }
}
