import { MaxSecurityTokenPersistService } from "@maxm/maxsecurity-angular-client";
import { AuthenticationMethod, MaxSecurityClientFactory } from "@maxm/maxsecurity-js-client";

export const authClientFactory = async (tokenPersistService: MaxSecurityTokenPersistService) => {
  console.log("create authClientFactory");
  const authConf = getAuthConf();
  let factory = new MaxSecurityClientFactory(authConf.authMethod);
  const storedToken = await tokenPersistService.readStoredToken();
  if (storedToken) {
    factory = factory.withToken(storedToken);
  }

  // Ensure we are in sync
  await factory.getAuthenticationMethod();

  return factory.create(authConf.properties);
};

export function getAuthConf() {
  console.log("Authenticate with Azure AD");
  return {
    authMethod: AuthenticationMethod.AZURE_AD,
    properties: {
      env: envFromHostname(location.hostname),
      clientId: "templateproject",
      appRoot: `${location.origin}/templateproject`,
    },
  };
}

function envFromHostname(hostname: string): string {
  if (hostname.startsWith("local")) {
    return "local";
  } else if (hostname.startsWith("dev-")) {
    return "dev";
  } else if (hostname.startsWith("int-")) {
    return "int";
  } else if (hostname.startsWith("sys-")) {
    return "sys";
  } else if (hostname.startsWith("stage-")) {
    return "stage";
  } else {
    return "prod";
  }
}
