package se.maxm.templateproject;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.jupiter.api.Test;

class TestArchitecture {
    @Test
    void domainDoesNotDependOnApplicationOrAdapterOrFramework() {
        noClasses()
                .that()
                .resideInAnyPackage("se.maxm.templateproject..domain..")
                .should()
                .dependOnClassesThat()
                .resideInAnyPackage(
                        "se.maxm.templateproject..application..",
                        "se.maxm.templateproject..adapter..",
                        //                        "org.springframework..",
                        "com.fasterxml.jackson..",
                        "javax.persistence..")
                .check(new ClassFileImporter().importPackages("se.maxm.templateproject.."));
    }

    @Test
    void applicationDoesNotDependOnAdapterOrFramework() {
        noClasses()
                .that()
                .resideInAnyPackage("se.maxm.templateproject..application..")
                .should()
                .dependOnClassesThat()
                .resideInAnyPackage(
                        "se.maxm.templateproject..adapter..",
                        //                  "org.springframework..",
                        "com.fasterxml.jackson..",
                        "javax.persistence..")
                .check(new ClassFileImporter().importPackages("se.maxm.templateproject.."));
    }
}
