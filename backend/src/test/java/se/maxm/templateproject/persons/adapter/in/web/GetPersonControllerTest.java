package se.maxm.templateproject.persons.adapter.in.web;

import static org.assertj.core.api.Assertions.assertThat;
import static se.maxm.templateproject.testtools.MockData.createPersonEntities;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.UUID;
import lombok.val;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;
import se.maxm.common.environment.MaxEnvironment;
import se.maxm.templateproject.frontendapimodel.AddressInfoDTO;
import se.maxm.templateproject.frontendapimodel.PersonDTO;
import se.maxm.templateproject.persons.adapter.out.persistence.PersonEntity;
import se.maxm.templateproject.persons.adapter.out.persistence.PersonJpaRepository;
import se.maxm.templateproject.testtools.SQLServerExtension;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"test", "nosecurity"})
// Make sure to use the following when using test containers:
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ExtendWith(SQLServerExtension.class)
// Ensure that Spring Data JPA gets re-initialized for the new jdbc url of the
// sqlserver test container
@DirtiesContext
class GetPersonControllerTest {
    @Autowired
    WebTestClient webClient;

    @Autowired
    PersonJpaRepository personJpaRepository;

    @Autowired
    ObjectMapper objectMapper;

    List<PersonEntity> personEntities;

    @BeforeAll
    static void beforeAll() {
        System.setProperty(MaxEnvironment.SYSTEM_PROPERTY_NAME, MaxEnvironment.LOCAL.getEnvironmentName());
    }

    @BeforeEach
    void setUp() {
        personJpaRepository.deleteAll();
        personEntities = createPersonEntities(6);
        personJpaRepository.saveAll(personEntities);
    }

    @AfterEach
    void tearDown() {
        personJpaRepository.deleteAll();
    }

    @Test
    void get_shouldReturnRequestedPerson() {
        // GIVEN
        // WHEN
        val personEntity = personEntities.get(0);
        val personDTO = webClient
                .get()
                .uri("/api/persons/%s".formatted(personEntity.getId()))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(PersonDTO.class)
                .returnResult()
                .getResponseBody();

        // THEN
        assertThat(personDTO)
                .isEqualTo(new PersonDTO(
                        personEntity.getId(),
                        personEntity.getFirstName(),
                        personEntity.getSurname(),
                        personEntity.getBirthDate(),
                        personEntity.getEmail().value(),
                        new AddressInfoDTO(
                                personEntity.getAddressInfo().street(),
                                personEntity.getAddressInfo().city(),
                                personEntity.getAddressInfo().zip())));
    }

    @Test
    void get_shouldRespondNotFoundForUnknownId() {
        // GIVEN
        // WHEN
        // THEN
        webClient
                .get()
                .uri("/api/persons/%s".formatted(UUID.randomUUID()))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isNotFound();
    }
}
