package se.maxm.templateproject.persons.adapter.in.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Mockito.when;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static se.maxm.templateproject.testtools.MockData.getJson;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import java.time.LocalDate;
import lombok.val;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.client.MockServerClient;
import org.mockserver.model.JsonBody;
import org.mockserver.springtest.MockServerTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;
import se.maxm.common.environment.MaxEnvironment;
import se.maxm.templateproject.config.AppConfig;
import se.maxm.templateproject.frontendapimodel.RegisterPersonCommandDTO;
import se.maxm.templateproject.persons.adapter.out.persistence.PersonEntity;
import se.maxm.templateproject.persons.adapter.out.persistence.PersonJpaRepository;
import se.maxm.templateproject.persons.domain.AddressInfo;
import se.maxm.templateproject.persons.domain.Email;
import se.maxm.templateproject.testtools.SQLServerExtension;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"test", "nosecurity"})
// Make sure to use the following when using test containers:
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ExtendWith(SQLServerExtension.class)
// Ensure that Spring Data JPA gets re-initialized for the new jdbc url of the
// sqlserver test container
@DirtiesContext
@MockServerTest("mock.server.url=http://localhost:${mockServerPort}")
class RegisterPersonControllerTest {
    @Value("${mock.server.url}")
    private URI mockServerUrl;

    MockServerClient mockServerClient;

    @Autowired
    WebTestClient webClient;

    @Autowired
    PersonJpaRepository personJpaRepository;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    AppConfig appConfig;

    @BeforeAll
    static void beforeAll() {
        System.setProperty(MaxEnvironment.SYSTEM_PROPERTY_NAME, MaxEnvironment.LOCAL.getEnvironmentName());
    }

    @BeforeEach
    void setUp() {
        when(appConfig.getAddressRegisterBaseUri()).thenReturn(mockServerUrl.toString());
        personJpaRepository.deleteAll();
    }

    @AfterEach
    void tearDown() {
        personJpaRepository.deleteAll();
    }

    @Test
    void post_shouldFetchAdressInfoFromAdressRegisterAndStorePerson() {
        // GIVEN
        val addressInfo = new AddressInfo("Gatan 42", "Staden", "42424");
        mockServerClient
                .when(request()
                        .withMethod("GET")
                        .withPath("/addresses")
                        .withQueryStringParameter("email", "test.testsson@maxm.se"))
                .respond(response().withStatusCode(200).withBody(new JsonBody(getJson(objectMapper, addressInfo))));

        // WHEN
        val response = webClient
                .post()
                .uri("/api/persons")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(new RegisterPersonCommandDTO(
                        "Test", "Testsson", LocalDate.now().minusYears(30), "test.testsson@maxm.se"))
                .exchange()
                .expectStatus()
                .isCreated()
                .returnResult(ResponseEntity.class);

        // THEN
        assertThat(personJpaRepository.findAll())
                .hasSize(1)
                .extracting(PersonEntity::getEmail, PersonEntity::getAddressInfo)
                .containsExactly(tuple(new Email("test.testsson@maxm.se"), addressInfo));
        assertThat(response.getResponseHeaders().getLocation().getPath())
                .isEqualTo("/templateproject/api/persons/%s"
                        .formatted(personJpaRepository.findAll().get(0).getId()));
    }
}
