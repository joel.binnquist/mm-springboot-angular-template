package se.maxm.templateproject.persons.adapter.out.persistence;

import static org.assertj.core.api.Assertions.assertThat;
import static se.maxm.templateproject.testtools.MockData.PERSON;

import lombok.val;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import se.maxm.templateproject.config.ObjectMapperConfig;
import se.maxm.templateproject.persons.domain.Person;
import se.maxm.templateproject.testtools.MockData;
import se.maxm.templateproject.testtools.SQLServerExtension;

@DataJpaTest
@Import({PersonRepository.class, ObjectMapperConfig.class})
@ActiveProfiles("test")
// Make sure to use the following when using test containers:
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ExtendWith(SQLServerExtension.class)
// Ensure that Spring Data JPA gets re-initialized for the new jdbc url of the
// sqlserver test container
@DirtiesContext
class PersonRepositoryTest {

    @Autowired
    PersonJpaRepository personJpaRepository;

    @Autowired
    PersonRepository personRepository;

    @BeforeEach
    void setUp() {
        personJpaRepository.deleteAll();
    }

    @AfterEach
    void tearDown() {
        personJpaRepository.deleteAll();
    }

    @Test
    void create_shouldPersistPersonAndGiveItANewId() {
        // GIVEN
        val person = PERSON;
        // WHEN
        val createdPerson = personRepository.createPerson(person);
        // THEN
        assertThat(createdPerson.getId()).isNotNull().isNotEqualTo(person.getId());
        assertThat(personJpaRepository.findById(createdPerson.getId()))
                .map(PersonEntity::getEmail)
                .get()
                .isEqualTo(person.getEmail());
    }

    @Test
    void loadAll_shouldReturnSpecifiedPage() {
        // GIVEN
        val personEntities = MockData.createPersonEntities(10);
        personJpaRepository.saveAll(personEntities);
        // WHEN
        val actualPage = personRepository.fetchPage(1, 3);
        // THEN
        assertThat(actualPage.items())
                .extracting(Person::getEmail)
                .containsExactly(
                        personEntities.get(3).getEmail(),
                        personEntities.get(4).getEmail(),
                        personEntities.get(5).getEmail());
    }

    @Test
    void getPerson_shouldReturnPersonById() {
        // GIVEN
        val personEntity = MockData.createPersonEntities(1).get(0);
        personJpaRepository.save(personEntity);
        // WHEN
        val actualPerson = personRepository.findById(personEntity.getId());
        // THEN
        assertThat(actualPerson)
                .isPresent()
                .get()
                .isEqualTo(new Person(
                        personEntity.getId(),
                        personEntity.getFirstName(),
                        personEntity.getSurname(),
                        personEntity.getBirthDate(),
                        personEntity.getEmail(),
                        personEntity.getAddressInfo()));
    }
}
