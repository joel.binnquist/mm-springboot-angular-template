package se.maxm.templateproject.persons.application;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static se.maxm.templateproject.testtools.MockData.PERSON;

import java.util.List;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.maxm.templateproject.common.Page;
import se.maxm.templateproject.persons.application.port.in.queries.PersonRef;
import se.maxm.templateproject.persons.application.port.out.LoadAllPersons;

@ExtendWith(MockitoExtension.class)
class ListAllPersonsTest {
    @Mock
    LoadAllPersons loadAllPersons;

    @InjectMocks
    ListAllPersons listAllPersons;

    @BeforeEach
    void setUp() {}

    @Test
    void getPage_shouldReturnTheSpecifiedPage() {
        // GIVEN
        val person = PERSON;
        when(loadAllPersons.fetchPage(3, 20)).thenReturn(new Page<>(3, 20, 999L, List.of(person)));
        // WHEN
        Page<PersonRef> actualResult = listAllPersons.getPage(3, 20);
        // THEN
        assertThat(actualResult)
                .extracting(Page::items, Page::pageNumber, Page::totalNumberOfItems, Page::requestedPageSize)
                .containsExactly(
                        List.of(new PersonRef(person.getId(), person.getFirstName(), person.getSurname())),
                        3,
                        999L,
                        20);
    }
}
