package se.maxm.templateproject.persons.application;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static se.maxm.templateproject.testtools.MockData.PERSON;

import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.maxm.templateproject.common.validation.ValidatorService;
import se.maxm.templateproject.persons.application.port.in.commands.RegisterPersonCommand;
import se.maxm.templateproject.persons.application.port.out.CreatePerson;
import se.maxm.templateproject.persons.application.port.out.FetchAddressInfo;
import se.maxm.templateproject.persons.domain.AddressInfo;
import se.maxm.templateproject.persons.domain.Person;
import se.maxm.templateproject.testtools.MockData;

@ExtendWith(MockitoExtension.class)
class RegisterPersonTest {
    @Mock
    CreatePerson createPerson;

    @Mock
    FetchAddressInfo fetchAddressInfo;

    @Mock
    ValidatorService validatorService;

    @InjectMocks
    RegisterPerson registerPerson;

    @Captor
    ArgumentCaptor<Person> personCaptor;

    @Test
    void register_shouldCreatePersonAndPutIntoRepo() {
        // GIVEN
        val command = new RegisterPersonCommand(
                PERSON.getFirstName(), PERSON.getSurname(), PERSON.getBirthDate(), PERSON.getEmail());
        val expectedCreatedPerson = PERSON;
        when(createPerson.createPerson(any(Person.class))).thenReturn(expectedCreatedPerson);
        AddressInfo addressInfo = MockData.getAddressInfo(PERSON);
        when(fetchAddressInfo.fetchAdressInfo(PERSON.getEmail())).thenReturn(addressInfo);

        // WHEN
        val actualCreatedPerson = registerPerson.registerPerson(command);

        // THEN
        verify(validatorService).validate(command);
        verify(createPerson).createPerson(personCaptor.capture());
        assertThat(personCaptor.getValue())
                .extracting(
                        Person::getFirstName,
                        Person::getSurname,
                        Person::getBirthDate,
                        Person::getEmail,
                        Person::getAddressInfo)
                .containsExactly(
                        command.firstName(), command.surname(), command.birthDate(), command.email(), addressInfo);
        assertThat(actualCreatedPerson).isSameAs(expectedCreatedPerson);
    }
}
