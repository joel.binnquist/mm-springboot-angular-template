package se.maxm.templateproject.persons.application;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static se.maxm.templateproject.testtools.MockData.PERSON;

import java.util.Optional;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import se.maxm.templateproject.persons.application.port.out.FindPerson;
import se.maxm.templateproject.persons.domain.Person;

@ExtendWith(MockitoExtension.class)
class GetPersonTest {
    @Mock
    FindPerson findPerson;

    @InjectMocks
    GetPerson getPerson;

    @BeforeEach
    void setUp() {}

    @Test
    void getPerson_shouldReturnThePersonById() {
        // GIVEN
        val person = PERSON;
        when(findPerson.findById(PERSON.getId())).thenReturn(Optional.of(PERSON));
        // WHEN
        Optional<Person> actualResult = getPerson.getPerson(PERSON.getId());
        // THEN
        assertThat(actualResult).isPresent().get().isSameAs(PERSON);
    }
}
