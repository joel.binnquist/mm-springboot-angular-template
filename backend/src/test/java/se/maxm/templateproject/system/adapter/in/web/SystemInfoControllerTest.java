package se.maxm.templateproject.system.adapter.in.web;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;
import se.maxm.common.environment.MaxEnvironment;
import se.maxm.templateproject.testtools.SQLServerExtension;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ExtendWith(SQLServerExtension.class)
class SystemInfoControllerTest {
    @Autowired
    WebTestClient webClient;

    String credentials;

    @BeforeAll
    static void beforeAll() {
        System.setProperty(MaxEnvironment.SYSTEM_PROPERTY_NAME, MaxEnvironment.LOCAL.getEnvironmentName());
    }

    @BeforeEach
    void setUp() {
        credentials = Base64.getEncoder().encodeToString("testadmin:testpassword".getBytes(StandardCharsets.UTF_8));
    }

    @AfterEach
    void tearDown() {}

    @Test
    void version_shouldNotRequireAuthentication() {
        webClient.get().uri("/sysinfo/version").exchange().expectStatus().isOk();
    }

    @Test
    void ping_shouldRequireAuthentication() {
        webClient
                .get()
                .uri("/sysinfo/ping")
                .header("authorization", "Basic bad-credentials")
                .exchange()
                .expectStatus()
                .isUnauthorized();
    }

    @Test
    void ping_shouldReturnPong() {
        webClient
                .get()
                .uri("/sysinfo/ping")
                .header("authorization", "Basic %s".formatted(credentials))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(String.class)
                .isEqualTo("pong!");
    }
}
