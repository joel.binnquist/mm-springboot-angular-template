package se.maxm.templateproject;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import se.maxm.common.environment.MaxEnvironment;

@SpringBootTest
class TemplateProjectApplicationTests {
    @BeforeAll
    static void beforeAll() {
        System.setProperty(MaxEnvironment.SYSTEM_PROPERTY_NAME, MaxEnvironment.LOCAL.getEnvironmentName());
    }

    @Test
    void contextLoads() {}
}
