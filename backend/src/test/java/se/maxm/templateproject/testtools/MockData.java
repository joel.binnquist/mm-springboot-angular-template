package se.maxm.templateproject.testtools;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.IntStream;
import lombok.experimental.UtilityClass;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import se.maxm.templateproject.persons.adapter.out.persistence.PersonEntity;
import se.maxm.templateproject.persons.domain.AddressInfo;
import se.maxm.templateproject.persons.domain.Email;
import se.maxm.templateproject.persons.domain.Person;

@UtilityClass
public class MockData {
    public static Person PERSON = createPersons(1).get(0);

    public static List<Person> createPersons(int n) {
        return IntStream.range(0, n)
                .mapToObj(i -> {
                    val firstName = "firstName" + i;
                    val surname = "surname" + i;
                    return new Person(
                            UUID.randomUUID(),
                            firstName,
                            surname,
                            LocalDate.now().minusYears(30).minusDays(i),
                            new Email("%s.%s@maxm.se".formatted(firstName, surname)),
                            getAddressInfo(surname));
                })
                .toList();
    }

    public static List<PersonEntity> createPersonEntities(int n) {
        return IntStream.range(0, n)
                .mapToObj(i -> {
                    val firstName = "firstName" + i;
                    val surname = "surname" + i;
                    return new PersonEntity(
                            UUID.randomUUID(),
                            firstName,
                            surname,
                            LocalDate.now().minusYears(30).minusDays(i),
                            new Email("%s.%s@maxm.se".formatted(firstName, surname)),
                            getAddressInfo(surname));
                })
                .toList();
    }

    public static AddressInfo getAddressInfo(Person person) {
        var surname = person.getSurname();
        return getAddressInfo(surname);
    }

    @NotNull
    private static AddressInfo getAddressInfo(String nameBase) {
        return new AddressInfo(
                "%sgatan %s".formatted(nameBase, new Random().nextInt(300)),
                "%sstaden".formatted(nameBase),
                "%s".formatted(new Random().nextInt(90000) + 10000));
    }

    public static <T> String getJson(ObjectMapper objectMapper, T value) {
        try {
            return objectMapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
