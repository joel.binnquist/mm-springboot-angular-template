package se.maxm.templateproject.testtools;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.testcontainers.containers.MSSQLServerContainer;
import org.testcontainers.utility.DockerImageName;

public class SQLServerExtension implements BeforeAllCallback, AfterAllCallback {
    private MSSQLServerContainer mssqlserver;

    @Override
    public void beforeAll(ExtensionContext context) {
        DockerImageName dockerImageName = DockerImageName.parse("mcr.microsoft.com/azure-sql-edge:latest")
                .asCompatibleSubstituteFor("mcr.microsoft.com/mssql/server");
        mssqlserver = new MSSQLServerContainer(dockerImageName).acceptLicense();
        mssqlserver.start();

        System.setProperty("spring.datasource.url", mssqlserver.getJdbcUrl());
        System.setProperty("spring.datasource.username", mssqlserver.getUsername());
        System.setProperty("spring.datasource.password", mssqlserver.getPassword());
    }

    @Override
    public void afterAll(ExtensionContext context) {
        mssqlserver.stop();
    }
}
