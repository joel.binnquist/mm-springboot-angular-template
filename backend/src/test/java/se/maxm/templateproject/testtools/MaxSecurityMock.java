package se.maxm.templateproject.testtools;

import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import lombok.RequiredArgsConstructor;
import org.mockserver.client.MockServerClient;
import org.mockserver.mock.Expectation;
import org.mockserver.model.JsonBody;
import se.maxm.security.api.TokenResponse;

@RequiredArgsConstructor
public class MaxSecurityMock {
    private final ObjectMapper objectMapper;
    private final MockServerClient mockServerClient;
    private Expectation[] expectations;

    public void start() {
        expectations = mockServerClient
                .when(request().withMethod("POST").withPath("/authentication/token"))
                .respond(response()
                        .withStatusCode(200)
                        .withBody(new JsonBody(getJson(TokenResponse.of("fejktoken", 30000)))));
    }

    private <T> String getJson(T token) {
        try {
            return objectMapper.writeValueAsString(token);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public void stop() {
        Arrays.stream(expectations).forEach(e -> mockServerClient.clear(e.getId()));
    }
}
