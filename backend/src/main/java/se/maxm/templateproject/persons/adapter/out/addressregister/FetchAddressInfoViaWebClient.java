package se.maxm.templateproject.persons.adapter.out.addressregister;

import java.time.Duration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import se.maxm.templateproject.config.AppConfig;
import se.maxm.templateproject.persons.application.port.out.FetchAddressInfo;
import se.maxm.templateproject.persons.domain.AddressInfo;
import se.maxm.templateproject.persons.domain.Email;

@Component
@Slf4j
@RequiredArgsConstructor
public class FetchAddressInfoViaWebClient implements FetchAddressInfo {
    private final AppConfig appConfig;
    private final WebClient webClient = WebClient.create();

    @Override
    public AddressInfo fetchAdressInfo(Email email) {
        return webClient
                .get()
                .uri("%s/addresses?email=%s".formatted(appConfig.getAddressRegisterBaseUri(), email.value()))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(AddressInfo.class)
                .block(Duration.ofSeconds(30));
    }
}
