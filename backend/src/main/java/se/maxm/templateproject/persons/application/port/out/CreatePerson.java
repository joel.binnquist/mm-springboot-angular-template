package se.maxm.templateproject.persons.application.port.out;

import se.maxm.templateproject.persons.domain.Person;

public interface CreatePerson {
    Person createPerson(Person person);
}
