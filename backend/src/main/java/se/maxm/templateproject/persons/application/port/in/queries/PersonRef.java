package se.maxm.templateproject.persons.application.port.in.queries;

import java.util.UUID;

public record PersonRef(UUID id, String firstName, String surName) {}
