package se.maxm.templateproject.persons.application.port.out;

import se.maxm.templateproject.persons.domain.AddressInfo;
import se.maxm.templateproject.persons.domain.Email;

public interface FetchAddressInfo {
    AddressInfo fetchAdressInfo(Email email);
}
