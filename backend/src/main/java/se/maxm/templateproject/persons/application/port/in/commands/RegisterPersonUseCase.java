package se.maxm.templateproject.persons.application.port.in.commands;

import javax.validation.Valid;
import org.springframework.validation.annotation.Validated;
import se.maxm.templateproject.persons.domain.Person;

@Validated
public interface RegisterPersonUseCase {
    Person registerPerson(@Valid RegisterPersonCommand registerPersonCommand);
}
