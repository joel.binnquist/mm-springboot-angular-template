package se.maxm.templateproject.persons.adapter.out.persistence;

import java.util.List;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PersonJpaRepository
        extends CrudRepository<PersonEntity, UUID>, PagingAndSortingRepository<PersonEntity, UUID> {
    List<PersonEntity> findAll();
}
