package se.maxm.templateproject.persons.application.port.in.queries;

import java.util.Optional;
import java.util.UUID;
import se.maxm.templateproject.persons.domain.Person;

public interface GetPersonUseCase {
    Optional<Person> getPerson(UUID id);
}
