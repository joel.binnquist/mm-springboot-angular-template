package se.maxm.templateproject.persons.application;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import se.maxm.templateproject.common.annotation.ApplicationComponent;
import se.maxm.templateproject.common.validation.ValidatorService;
import se.maxm.templateproject.persons.application.port.in.commands.RegisterPersonCommand;
import se.maxm.templateproject.persons.application.port.in.commands.RegisterPersonUseCase;
import se.maxm.templateproject.persons.application.port.out.CreatePerson;
import se.maxm.templateproject.persons.application.port.out.FetchAddressInfo;
import se.maxm.templateproject.persons.domain.Person;

@ApplicationComponent
@RequiredArgsConstructor
@Slf4j
public class RegisterPerson implements RegisterPersonUseCase {
    private final CreatePerson createPerson;
    private final FetchAddressInfo fetchAddressInfo;

    private final ValidatorService validator;

    @Override
    public Person registerPerson(RegisterPersonCommand registerPersonCommand) {
        validator.validate(registerPersonCommand);
        val address = fetchAddressInfo.fetchAdressInfo(registerPersonCommand.email());
        return createPerson.createPerson(Person.builder()
                .firstName(registerPersonCommand.firstName())
                .surname(registerPersonCommand.surname())
                .birthDate(registerPersonCommand.birthDate())
                .email(registerPersonCommand.email())
                .addressInfo(address)
                .build());
    }
}
