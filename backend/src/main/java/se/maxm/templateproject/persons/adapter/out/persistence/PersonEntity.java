package se.maxm.templateproject.persons.adapter.out.persistence;

import java.time.LocalDate;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import se.maxm.templateproject.persons.domain.AddressInfo;
import se.maxm.templateproject.persons.domain.Email;

@Entity
@Table(name = "person")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PersonEntity {
    @Id
    private UUID id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "surname")
    private String surname;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @Column(name = "email")
    @Convert(converter = EmailAttributeConverter.class)
    private Email email;

    @Column(name = "address_info")
    @Convert(converter = AddressInfoAttributeConverter.class)
    private AddressInfo addressInfo;
}
