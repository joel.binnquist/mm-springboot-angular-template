package se.maxm.templateproject.persons.application.port.in.commands;

import java.time.LocalDate;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import se.maxm.templateproject.persons.domain.Email;

public record RegisterPersonCommand(
        @NotBlank String firstName,
        @NotBlank String surname,
        @NotNull @Past LocalDate birthDate,
        @NotNull @Valid Email email) {}
