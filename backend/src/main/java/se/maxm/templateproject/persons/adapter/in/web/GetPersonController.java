package se.maxm.templateproject.persons.adapter.in.web;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.UUID;
import java.util.function.Function;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.maxm.templateproject.common.exception.NotFoundException;
import se.maxm.templateproject.frontendapimodel.AddressInfoDTO;
import se.maxm.templateproject.frontendapimodel.PersonDTO;
import se.maxm.templateproject.persons.application.port.in.queries.GetPersonUseCase;
import se.maxm.templateproject.persons.domain.Person;

@RestController
@Api("Example of API for getting a person")
@RequestMapping(path = "/api/persons", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
@RequiredArgsConstructor
public class GetPersonController {
    private final GetPersonUseCase getPersonUseCase;

    @Operation(
            summary = "Get person by ID",
            operationId = "getPersonById",
            responses = {
                @ApiResponse(responseCode = "200"),
                @ApiResponse(responseCode = "404", description = "No person found for that ID"),
                @ApiResponse(responseCode = "500", content = @Content, description = "Technical error"),
            })
    @GetMapping("/{id}")
    public PersonDTO get(@PathVariable String id) {
        return getPersonUseCase
                .getPerson(UUID.fromString(id))
                .map(toDto())
                .orElseThrow(() -> new NotFoundException("Could not find person by id:%s".formatted(id)));
    }

    private Function<Person, PersonDTO> toDto() {
        return person -> new PersonDTO(
                person.getId(),
                person.getFirstName(),
                person.getSurname(),
                person.getBirthDate(),
                person.getEmail().value(),
                new AddressInfoDTO(
                        person.getAddressInfo().street(),
                        person.getAddressInfo().city(),
                        person.getAddressInfo().zip()));
    }
}
