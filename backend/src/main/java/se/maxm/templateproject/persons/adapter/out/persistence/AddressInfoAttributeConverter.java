package se.maxm.templateproject.persons.adapter.out.persistence;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.RequiredArgsConstructor;
import se.maxm.templateproject.common.exception.ApplicationJsonMappingException;
import se.maxm.templateproject.persons.domain.AddressInfo;

@Converter
@RequiredArgsConstructor
public class AddressInfoAttributeConverter implements AttributeConverter<AddressInfo, String> {
    private final ObjectMapper objectMapper;

    @Override
    public String convertToDatabaseColumn(AddressInfo attribute) {
        try {
            return objectMapper.writeValueAsString(attribute);
        } catch (JsonProcessingException e) {
            throw new ApplicationJsonMappingException(
                    "Failed to convert AddressInfo %s to string".formatted(attribute), e);
        }
    }

    @Override
    public AddressInfo convertToEntityAttribute(String dbData) {
        try {
            return objectMapper.readValue(dbData, AddressInfo.class);
        } catch (JsonProcessingException e) {
            throw new ApplicationJsonMappingException(
                    "Failed to convert string %s to AddressInfo".formatted(dbData), e);
        }
    }
}
