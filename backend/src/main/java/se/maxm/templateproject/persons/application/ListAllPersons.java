package se.maxm.templateproject.persons.application;

import java.util.function.Function;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import se.maxm.templateproject.common.Page;
import se.maxm.templateproject.common.annotation.ApplicationComponent;
import se.maxm.templateproject.persons.application.port.in.queries.ListAllPersonsUseCase;
import se.maxm.templateproject.persons.application.port.in.queries.PersonRef;
import se.maxm.templateproject.persons.application.port.out.LoadAllPersons;
import se.maxm.templateproject.persons.domain.Person;

@ApplicationComponent
@RequiredArgsConstructor
@Slf4j
public class ListAllPersons implements ListAllPersonsUseCase {
    private final LoadAllPersons loadAllPersons;

    @Override
    public Page<PersonRef> getPage(int pageNumber, int requestedPageSize) {
        val personPage = loadAllPersons.fetchPage(pageNumber, requestedPageSize);
        return new Page<>(
                pageNumber,
                requestedPageSize,
                personPage.totalNumberOfItems(),
                personPage.items().stream().map(toPersonRef()).toList());
    }

    private Function<Person, PersonRef> toPersonRef() {
        return p -> new PersonRef(p.getId(), p.getFirstName(), p.getSurname());
    }
}
