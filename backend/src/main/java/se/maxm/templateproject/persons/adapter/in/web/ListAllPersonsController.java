package se.maxm.templateproject.persons.adapter.in.web;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.Optional;
import java.util.function.Function;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.maxm.templateproject.common.Page;
import se.maxm.templateproject.frontendapimodel.PageDTO;
import se.maxm.templateproject.frontendapimodel.PersonRefDTO;
import se.maxm.templateproject.persons.application.port.in.queries.ListAllPersonsUseCase;
import se.maxm.templateproject.persons.application.port.in.queries.PersonRef;

@RestController
@Api("Example of API for listing persons")
@RequestMapping(path = "/api/persons", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
@RequiredArgsConstructor
public class ListAllPersonsController {
    private final ListAllPersonsUseCase listAllPersonsUseCase;

    @Operation(
            summary = "List all persons (paged)",
            operationId = "listAllPersons",
            responses = {
                @ApiResponse(responseCode = "200"),
                @ApiResponse(responseCode = "500", content = @Content, description = "Technical error"),
            })
    @GetMapping("")
    public PageDTO<PersonRefDTO> get(
            @RequestParam(defaultValue = "0") int pageNumber,
            @RequestParam(defaultValue = "20") int requestedPageSize) {
        return Optional.of(listAllPersonsUseCase.getPage(pageNumber, requestedPageSize))
                .map(toDto())
                .orElseThrow();
    }

    private Function<Page<PersonRef>, PageDTO<PersonRefDTO>> toDto() {
        return page -> new PageDTO<>(
                page.pageNumber(),
                page.requestedPageSize(),
                page.totalNumberOfItems(),
                page.items().stream()
                        .map(p -> new PersonRefDTO(p.id(), p.firstName(), p.surName()))
                        .toList());
    }
}
