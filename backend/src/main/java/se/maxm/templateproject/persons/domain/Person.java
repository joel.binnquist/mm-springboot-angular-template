package se.maxm.templateproject.persons.domain;

import java.time.LocalDate;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
public class Person {
    @NotNull
    private UUID id;

    @NotBlank
    private String firstName;

    @NotBlank
    private String surname;

    @NotNull
    @Past
    private LocalDate birthDate;

    @NotNull
    @Valid
    private Email email;

    @NotNull
    private AddressInfo addressInfo;
}
