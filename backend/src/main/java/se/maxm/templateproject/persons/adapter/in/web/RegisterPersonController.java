package se.maxm.templateproject.persons.adapter.in.web;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.function.Function;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.maxm.templateproject.frontendapimodel.RegisterPersonCommandDTO;
import se.maxm.templateproject.persons.application.port.in.commands.RegisterPersonCommand;
import se.maxm.templateproject.persons.application.port.in.commands.RegisterPersonUseCase;
import se.maxm.templateproject.persons.domain.Email;

@RestController
@Api("Example of API for register a new person")
@RequestMapping(path = "/api/persons", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
@RequiredArgsConstructor
public class RegisterPersonController {
    private final RegisterPersonUseCase registerPersonUseCase;

    @Operation(
            summary = "Register a new person",
            operationId = "registerPerson",
            responses = {
                @ApiResponse(responseCode = "200"),
                @ApiResponse(responseCode = "500", content = @Content, description = "Technical error"),
            })
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> post(@RequestBody RegisterPersonCommandDTO registerPersonCommand) {
        val createdPerson = registerPersonUseCase.registerPerson(toApplication().apply(registerPersonCommand));
        val location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdPerson.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    private Function<RegisterPersonCommandDTO, RegisterPersonCommand> toApplication() {
        return dto ->
                new RegisterPersonCommand(dto.firstName(), dto.surname(), dto.birthDate(), new Email(dto.email()));
    }
}
