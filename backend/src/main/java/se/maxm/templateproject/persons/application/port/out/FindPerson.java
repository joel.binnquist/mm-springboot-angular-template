package se.maxm.templateproject.persons.application.port.out;

import java.util.Optional;
import java.util.UUID;
import se.maxm.templateproject.persons.domain.Person;

public interface FindPerson {
    Optional<Person> findById(UUID id);
}
