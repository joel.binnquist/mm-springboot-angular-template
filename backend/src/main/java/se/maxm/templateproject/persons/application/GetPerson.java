package se.maxm.templateproject.persons.application;

import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import se.maxm.templateproject.common.annotation.ApplicationComponent;
import se.maxm.templateproject.persons.application.port.in.queries.GetPersonUseCase;
import se.maxm.templateproject.persons.application.port.out.FindPerson;
import se.maxm.templateproject.persons.domain.Person;

@ApplicationComponent
@RequiredArgsConstructor
@Slf4j
public class GetPerson implements GetPersonUseCase {
    private final FindPerson findPerson;

    @Override
    public Optional<Person> getPerson(UUID id) {
        return findPerson.findById(id);
    }
}
