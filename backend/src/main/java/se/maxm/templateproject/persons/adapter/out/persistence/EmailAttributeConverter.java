package se.maxm.templateproject.persons.adapter.out.persistence;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import se.maxm.templateproject.persons.domain.Email;

@Converter
public class EmailAttributeConverter implements AttributeConverter<Email, String> {
    @Override
    public String convertToDatabaseColumn(Email attribute) {
        return attribute.value();
    }

    @Override
    public Email convertToEntityAttribute(String dbData) {
        return new Email(dbData);
    }
}
