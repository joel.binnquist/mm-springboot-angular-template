package se.maxm.templateproject.persons.adapter.out.persistence;

import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import se.maxm.templateproject.common.Page;
import se.maxm.templateproject.persons.application.port.out.CreatePerson;
import se.maxm.templateproject.persons.application.port.out.FindPerson;
import se.maxm.templateproject.persons.application.port.out.LoadAllPersons;
import se.maxm.templateproject.persons.domain.Person;

@Component
@RequiredArgsConstructor
public class PersonRepository implements CreatePerson, LoadAllPersons, FindPerson {
    private final PersonJpaRepository personJpaRepository;

    @Override
    public Person createPerson(Person person) {
        return Optional.of(personJpaRepository.save(toEntity()
                        .apply(person.toBuilder().id(UUID.randomUUID()).build())))
                .map(toDomain())
                .orElseThrow();
    }

    @Override
    public Page<Person> fetchPage(int pageNumber, int requestedPageSize) {
        val page = personJpaRepository.findAll(
                PageRequest.of(pageNumber, requestedPageSize, Sort.Direction.ASC, "surname"));
        return new Page<>(
                pageNumber,
                requestedPageSize,
                page.getTotalElements(),
                page.getContent().stream().map(toDomain()).toList());
    }

    @Override
    public Optional<Person> findById(UUID id) {
        return personJpaRepository.findById(id).map(toDomain());
    }

    private Function<Person, PersonEntity> toEntity() {
        return person -> new PersonEntity(
                person.getId(),
                person.getFirstName(),
                person.getSurname(),
                person.getBirthDate(),
                person.getEmail(),
                person.getAddressInfo());
    }

    private Function<PersonEntity, Person> toDomain() {
        return personEntity -> new Person(
                personEntity.getId(),
                personEntity.getFirstName(),
                personEntity.getSurname(),
                personEntity.getBirthDate(),
                personEntity.getEmail(),
                personEntity.getAddressInfo());
    }
}
