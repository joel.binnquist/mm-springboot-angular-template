package se.maxm.templateproject.persons.domain;

public record Email(@javax.validation.constraints.Email String value) {}
