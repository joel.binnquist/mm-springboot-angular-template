package se.maxm.templateproject.persons.application.port.out;

import se.maxm.templateproject.common.Page;
import se.maxm.templateproject.persons.domain.Person;

public interface LoadAllPersons {
    Page<Person> fetchPage(int pageNumber, int requestedPageSize);
}
