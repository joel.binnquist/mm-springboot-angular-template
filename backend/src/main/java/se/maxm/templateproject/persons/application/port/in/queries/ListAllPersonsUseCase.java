package se.maxm.templateproject.persons.application.port.in.queries;

import se.maxm.templateproject.common.Page;

public interface ListAllPersonsUseCase {
    Page<PersonRef> getPage(int pageNumber, int requestedPageSize);
}
