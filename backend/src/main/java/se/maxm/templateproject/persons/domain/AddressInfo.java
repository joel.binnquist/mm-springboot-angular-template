package se.maxm.templateproject.persons.domain;

public record AddressInfo(String street, String city, String zip) {}
