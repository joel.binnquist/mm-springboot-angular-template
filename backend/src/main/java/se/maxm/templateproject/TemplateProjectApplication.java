package se.maxm.templateproject;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import se.maxm.common.environment.MaxEnvironment;

@Slf4j
@SpringBootApplication
public class TemplateProjectApplication {

    public static final String APPLICATION_NAME = "templateproject";

    public static void main(String[] args) {
        System.setProperty("maxm.app", APPLICATION_NAME);
        log.info("App version: %s".formatted(System.getProperty("app.version", "unknown")));

        if (System.getProperty("spring.profiles.active") == null) {
            log.error("NO ACTIVE PROFILE IS SET. Requires -Dspring.profiles.active parameter to"
                    + " start. WILL TERMINATE!  <<<========");
            System.exit(1);
        }

        if (System.getProperty(MaxEnvironment.SYSTEM_PROPERTY_NAME) == null) {
            System.setProperty(MaxEnvironment.SYSTEM_PROPERTY_NAME, System.getProperty("spring.profiles.active"));
        }

        SpringApplication.run(TemplateProjectApplication.class, args);
    }
}
