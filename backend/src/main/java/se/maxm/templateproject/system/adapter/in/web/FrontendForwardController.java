package se.maxm.templateproject.system.adapter.in.web;

import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * This controller serves the frontend angular application.
 *
 * See section 'Using “Natural” Routes' in
 * <a href="https://spring.io/blog/2015/05/13/modularizing-the-client-angular-js-and-spring-security-part-vii">Modularizing the Client: Angular JS and Spring Security Part VII</a>
 * for more details.
 *
 * This controller will catch all routes that are not explicitly mapped in other controllers.
 *
 * Note, the request mapping expression requires <code>spring.mvc.pathmatch.matching-strategy=ant_path_matcher</code> in the application
 * configuration. It will not work with the {@link org.springframework.web.util.pattern.PathPattern}, which is default from Spring 5.0.
 */
@Controller
@Slf4j
public class FrontendForwardController {

    @GetMapping("/**/{path:[^.]*}")
    public String forwardFrontendRequests(HttpServletRequest req, @PathVariable("path") String path) {
        log.trace("Forwarding path: " + req.getRequestURI() + " to /index.html");
        return "forward:/index.html";
    }
}
