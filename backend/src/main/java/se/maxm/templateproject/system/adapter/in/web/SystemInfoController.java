package se.maxm.templateproject.system.adapter.in.web;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.maxm.templateproject.frontendapimodel.VersionInfoDTO;

@RestController
@RequestMapping(path = "/sysinfo", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
@RequiredArgsConstructor
public class SystemInfoController {
    private final BuildProperties buildProperties;

    @GetMapping("/version")
    @ApiOperation(value = "Get application version")
    public VersionInfoDTO getVersionInfo() {
        return new VersionInfoDTO(buildProperties.getVersion(), buildProperties.getTime());
    }

    @GetMapping(value = "/ping", produces = MediaType.TEXT_PLAIN_VALUE)
    @ApiOperation(value = "Check that application works")
    public String ping() {
        return "pong!";
    }
}
