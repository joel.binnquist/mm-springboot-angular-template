package se.maxm.templateproject.common;

import java.util.List;

public record Page<T>(int pageNumber, int requestedPageSize, long totalNumberOfItems, List<T> items) {}
