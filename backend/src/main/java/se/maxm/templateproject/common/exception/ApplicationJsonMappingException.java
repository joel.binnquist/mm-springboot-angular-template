package se.maxm.templateproject.common.exception;

public class ApplicationJsonMappingException extends RuntimeException {
    public ApplicationJsonMappingException(String message, Throwable cause) {
        super(message, cause);
    }
}
