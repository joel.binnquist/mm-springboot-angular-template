package se.maxm.templateproject.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class AppConfig {
    @Value("${address-register.base-uri:localhost:1880}") // Default to the NodeRED-based mock
    String addressRegisterBaseUri;
}
