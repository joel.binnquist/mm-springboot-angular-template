package se.maxm.templateproject.config;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import se.maxm.security.domain.claims.MaxSecurityAuthenticationMethod;
import se.maxm.security.domain.claims.MaxSecurityScope;
import se.maxm.security.properties.MaxSecurityProperties;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationCodeGrant;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.ClientCredentialsGrant;
import springfox.documentation.service.OAuth;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.service.TokenEndpoint;
import springfox.documentation.service.TokenRequestEndpoint;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .securityContexts(securityContexts())
                .securitySchemes(List.of(securityScheme()))
                .select()
                .apis(RequestHandlerSelectors.basePackage("se.maxm.templateproject"))
                .apis(isNotFrontendForwardController())
                .paths(PathSelectors.any())
                .build();
    }

    private static Predicate<RequestHandler> isNotFrontendForwardController() {
        // Ignore se.maxm.templateproject.system.adapter.in.web.FrontendForwardController.forwardFrontendRequests
        return r -> !"forwardFrontendRequests".equals(r.getName());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Templateproject REST APIs",
                "APIs for the Templateproject.",
                null,
                null,
                null,
                null,
                null,
                Collections.emptyList());
    }

    private List<SecurityContext> securityContexts() {
        return List.of(SecurityContext.builder()
                .securityReferences(authWithScope(MaxSecurityScope.INTERNAL))
                .forPaths(PathSelectors.ant("/templateproject/api/**"))
                .build());
    }

    private List<SecurityReference> authWithScope(MaxSecurityScope scope) {
        return List.of(new SecurityReference(
                "MaxSecurity",
                new AuthorizationScope[] {new AuthorizationScope(scope.getScopeValue(), scope.getDescription())}));
    }

    private SecurityScheme securityScheme() {
        return new OAuth(
                "MaxSecurity",
                Stream.of(MaxSecurityScope.values())
                        .map(scope -> new AuthorizationScope(scope.getScopeValue(), scope.getDescription()))
                        .toList(),
                List.of(
                        new ClientCredentialsGrant(MaxSecurityProperties.get()
                                .getEndpoints()
                                .getTokenEndpoint()
                                .toString()),
                        new AuthorizationCodeGrant(
                                new TokenRequestEndpoint(
                                        MaxSecurityProperties.get()
                                                        .getEndpoints()
                                                        .getAuthorizationEndpoint()
                                                + "/"
                                                + MaxSecurityAuthenticationMethod.AZURE_AD.getMethodValue(),
                                        null,
                                        null),
                                new TokenEndpoint(
                                        MaxSecurityProperties.get()
                                                .getEndpoints()
                                                .getTokenEndpoint()
                                                .toString(),
                                        "access_token"))));
    }
}
