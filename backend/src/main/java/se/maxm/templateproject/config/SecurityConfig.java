package se.maxm.templateproject.config;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.filter.ForwardedHeaderFilter;
import se.maxm.security.adapter.MaxSecurityApiConfigurerAdapter;
import se.maxm.security.adapter.MaxSecurityGeneralConfigurerAdapter;
import se.maxm.security.authorization.config.MaxMethodSecurityConfig;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    protected SecurityConfig() {}

    /**
     * Enable annotation based security using
     * <a href="https://docs.spring.io/spring-security/reference/5.7.1/servlet/authorization/method-security.html">Spring Security Method Security</a>.
     * @see .
     */
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    @Profile("!test and !nosecurity")
    public static class MethodSecurityConfig extends MaxMethodSecurityConfig {}

    /**
     * This configuration disables all security when the profile <code>nosecurity</code> is active.
     * <p>
     * Note that the order is higher than the other configurations, so it will override them when active.
     * </p>
     * <p>
     * Also note that it is ignored in <code>prod</code>.
     * </p>
     */
    @Configuration
    @Order(SecurityProperties.BASIC_AUTH_ORDER - 15)
    @Profile("nosecurity & !prod")
    public static class NoSecurityAdapter extends MaxSecurityApiConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            defaultConfig(http)
                    .cors()
                    .disable()
                    .authorizeRequests()
                    .anyRequest()
                    .permitAll();
        }
    }

    /**
     * This configuration enables OCI authentication using MaxSecurity on /api/***.
     */
    @Configuration
    @Order(SecurityProperties.BASIC_AUTH_ORDER - 10)
    public static class ApiAuthAdapter extends MaxSecurityApiConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            defaultConfig(http)
                    .addFilterBefore(new ForwardedHeaderFilter(), CorsFilter.class)
                    .requestMatchers()
                    .antMatchers("/api/**")
                    .and()
                    .authorizeRequests()
                    .antMatchers("/api/**")
                    .authenticated();
        }
    }

    /**
     * This configuration sets up Basic Auth with roles and passwords for the actuator and sysinfo endpoints.
     * <p>
     * The route /application/health has some special configuration though.
     * For that endpoint it is possible to get minimal health status without authentication.
     * Config in application.yml specifies that no details are allowed unless authenticated.
     * </p>
     * <p>
     * Also, the route /sysinfo/version is available without authentication.
     * </p>
     */
    @Configuration
    @Order(SecurityProperties.BASIC_AUTH_ORDER - 8)
    public static class ActuatorConfigurationAdapter extends MaxSecurityGeneralConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            basicAuthConfig(http, "Actuator")
                    // Allow fetching minimal health and version info without authentication
                    .authorizeRequests()
                    .antMatchers(HttpMethod.GET, "/application/health", "/sysinfo/version")
                    .permitAll()
                    .and()
                    // All other actuator endpoints require basic authentication
                    .authorizeRequests()
                    .antMatchers("/application/**", "/sysinfo/**")
                    .authenticated();
        }
    }
}
