package se.maxm.templateproject.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import se.maxm.templateproject.common.annotation.ApplicationComponent;

/**
 * Configure the customized component scanning here instead of in the
 * main application class, to avoid problem with too broad scanning
 * in the test classes of slices (e.g. in @DataJpaTest annotated classes).
 * <p>
 * See <a href="https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#features.testing.spring-boot-applications.user-configuration-and-slicing">Spring Boot Reference Documentation: User Configuration and Slicing</a>.
 * </p>
 */
@Configuration
@ComponentScan(
        basePackages =
                "se.maxm.templateproject", // Must be specified since this class is not in the same package as the
        // application main class
        includeFilters = {
            @ComponentScan.Filter(
                    type = FilterType.ANNOTATION,
                    classes = {ApplicationComponent.class})
        })
public class ComponentScanConfiguration {}
