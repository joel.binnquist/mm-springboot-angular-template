Backend
=======
The backend implements a simple example application for managing persons.

It has the following features:
- structured according to 
  - [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
  - [Hexagonal Architecture](https://alistair.cockburn.us/hexagonal-architecture/)
  - **NOTE!!!** It is optional to adopt this architecture. For simpler 
    applications with few use cases it is probably overkill, but it is a good 
    architecture to grow in. This is especially important in agile development
    where the application grows incrementally.
  - Se details in [Application Documentation](../doc/README.md).
- secured with [MaxSecurity Springboot](https://gitlab.com/maxmatthiessen/libraries/maxsecurity-client/-/blob/c066915d237e6357e46ac2356c153c0f6db456a5/maxsecurity-spring-boot/README.md)
  - See [SecurityConfig.java](./src/main/java/se/maxm/templateproject/config/SecurityConfig.java)
  - Also, to run the application without security (for example in tests and 
    during development), activate the profile _nosecurity_. 
- REST API documented with [Springfox](https://springfox.github.io/springfox/docs/current/)
- application input validated using [JSR-380 Bean validation](https://jcp.org/en/jsr/detail?id=380)
- application unit tests uses [Mockito](https://site.mockito.org/) and 
  [AssertJ](https://assertj.github.io/doc/)
  - The principle is to implement exhaustive test within the 
    [application](./src/main/java/se/maxm/templateproject/persons/application) 
    package where the business logic resides. 
  - The unit tests uses mocks (Mockito) for all interfaces in 
    [output ports](./src/main/java/se/maxm/templateproject/persons/application/port/out)
  - See examples in [src/test/java/se/maxm/templateproject/persons/application](./src/test/java/se/maxm/templateproject/persons/application)
- JPA repository tests with [Auto-configured Data JPA Tests](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#features.testing.spring-boot-applications.autoconfigured-spring-data-jpa)
  - This is specific to persistence adapters. For these we implement `@DataJPATest`-
    annotated tests that are faster that full-fledged `@SpringbootTest`-annotated
    tests.
  - The use test containers for the "real" database.
  - They shall focus only on repository operations and will test
    - That liquibase scripts have executed correctly
    - That adaptions to the underlying persistence framework woirks as expected
      - Specific custom queries
      - JPA Attribute converters
      - JPA relations mappings
      - etc.
  - See example in [src/test/java/se/maxm/templateproject/persons/adapter/out/persistence/PersonRepositoryTest.java](./src/test/java/se/maxm/templateproject/persons/adapter/out/persistence/PersonRepositoryTest.java)
- integration test with 
  - [WebTestClient](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#features.testing.spring-boot-applications.with-running-server) 
  - [MockServer using @MockServerTest ](https://www.mock-server.com/mock_server/running_mock_server.html#spring_test_exec_listener) 
  - [TestContainers](https://www.testcontainers.org/)
  - These are few (one per REST method) and tests the full integration of web,
    persistence, external systems and application/domain
  - Nothing is mocked except the external system, which is mocked on HTTP level
    using _MockServer_
  - See examples in [src/test/java/se/maxm/templateproject/persons/adapter/in/web](./src/test/java/se/maxm/templateproject/persons/adapter/in/web)
- architecture testing with [ArchUnit](https://www.archunit.org/)

Architecture
------------
As mentioned above the backend architecture is described in [Application Documentation](../doc/README.md).

The _domain_ and _application_ layers are implemented in a way to avoid 
dependencies to
- Spring (which forces one to avoid adapter layer code to leak in to the 
  business logic). In order to still be able to use dependency  injection the 
  custom [@ApplicationComponent](./src/main/java/se/maxm/templateproject/common/annotation/ApplicationComponent.java)
  annotation is introduced.
- persistence solution (which, beside avoiding persistence layer specific to 
  leak in to the business logic, facilitates the option to alter the persistence 
  solution without affecting business logic).
- Jackson (since that clutters code and also binds to external representation,
  which belongs to the adapter layer).

The constraints above are checked using _ArchUnit_.

Bean Validation
---------------
The classes making up the input parameters of the use cases in the 
_application_ layer are validated using _JSR-380 Bean Validation_. 
This introduces a minimal dependency to annotations, but the actual 
implementation of the validation is hidden from the _application_ layer using 
[ValidatorService.java](./src/main/java/se/maxm/templateproject/common/validation/ValidatorService.java).

Application Configuration
-------------------------
Application properties are picked up in the [AppConfig.java](./src/main/java/se/maxm/templateproject/config/AppConfig.java)
class.


